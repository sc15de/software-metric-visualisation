# Software Metric Visualisation  
## Description  
The main purpose of the software metric visualisation software is to parse and compute a selection of software metrics for a given software product and generate a 3D visualisation of the class dependency structure which can be transformed in-order to communicate the values of the chosen software metrics on a per-class basis. The software can also be used to visualise the changes in the values of the chosen software metrics through each milestone of the input software product. The visualisation of the software product along with any included milestones can be exported to a project file bearing the .smf extension which contains the data representation of the software product. This project file can be loaded at any time to re-create the environment without needing to parse each milestone of the software product’s source code.  

**Please read the included software manual for futher details**

## System Requirements  
- Java: 8 (1.8.0_181)
- OS: Windows Vista/7/8/10, Ubuntu 14.04 or later  
- Processor: 2.0 GHz 64-bit or better  
- Memory: 2 GB RAM  
- Graphics: Must support OpenGL 2.1 or higher, Intel HD 3000 or better  
- Storage: 100 MB available space  

## Installation  
1. Extract the compressed ZIP folder “Software_Metric_Visualisation”  
2. Open the extracted folder and run the file “Software_Metric_Visualisation.jar”  
Run with console output:  
3. Navigate to the extracted folder in a terminal window  
4. Enter command “java -jar Software_Metric_Visualisation.jar”  
