package inheritanceexample;

public class Parent {
    public void Parent(){}
    public void parentMethod1(){ }
    public void parentMethod2(){ }
    public void parentMethod3(){ }
    public void parentMethod4(){ }
    public void parentMethod5(){ }
    public void parentMethod6(){ }
    public void parentMethod7(){ }
    public void parentMethod8(){ }
    public void parentMethod9(){ }
    public void parentMethod10(){ }
    public void parentMethod11(){ }
    public void parentMethod12(){ }
    public void parentMethod13(){ }
    public void parentMethod14(){ }
    public void parentMethod15(){ }
    public void parentMethod16(){ }
    public void parentMethod17(){ }
    public void parentMethod18(){ }
}

class Child1 extends Parent {
    public Child1(){}
    public void childMethod1(){}
    public void childMethod2(){}
    public void childMethod3(){}
    public void childMethod4(){}
    public void childMethod5(){}
    public void childMethod6(){}
    public void childMethod7(){}
    public void childMethod8(){}
    public void childMethod9(){}
}


class Child2 extends Parent{
    public Child2(){}
    public void childMethod1(){}
    public void childMethod2(){}
    public void childMethod3(){}
    public void childMethod4(){}
}