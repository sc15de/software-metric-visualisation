package dependencycycleexample;

public class Origin {
    Cycle3 cycle3 = new Cycle3();
    public Origin(){};
    public void method1(){}
    public void method2(){}
    public void method3(){}
    public void method4(){}
    public void method5(){}
    public void method6(){}
    public void method7(){}
    public void method8(){}
    public void method9(){}
}


class Cycle1 {
    Origin origin = new Origin();
    public Cycle1(){};
    public void method1(){}
    public void method2(){}
    public void method3(){}
    public void method4(){}
    public void method5(){}
    public void method6(){}
    public void method7(){}
    public void method8(){}
    public void method9(){}
}

class Cycle2 {
    Cycle1 cycle1 = new Cycle1();
    public Cycle2(){};
    public void method1(){}
    public void method2(){}
    public void method3(){}
    public void method4(){}
    public void method5(){}
}

class Cycle3 extends Cycle2{
    public Cycle3(){};
    public void method1(){}
    public void method2(){}
    public void method3(){}
    public void method4(){}
    public void method5(){}
    public void method6(){}
    public void method7(){}
    public void method8(){}
    public void method9(){}
    public void method10(){}
    public void method11(){}
    public void method12(){}
    public void method13(){}
    public void method14(){}
    public void method15(){}
    public void method16(){}
    public void method17(){}
    public void method18(){}
}



class NotCycle{
    Origin origin = new Origin();
    public NotCycle(){};
    public void method1(){}
    public void method2(){}
    public void method3(){}
    public void method4(){}
    public void method5(){}
    public void method6(){}
    public void method7(){}
    public void method8(){}
    public void method9(){}
}