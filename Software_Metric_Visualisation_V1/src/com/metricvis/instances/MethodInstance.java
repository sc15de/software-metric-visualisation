package com.metricvis.instances;


//Representation of a method instance
public class MethodInstance extends InstanceBase {

    //Number of code paths in the method
    private int logicalOperations = 1;

    //Number of method arguments
    private int numArguments = 0;

    //Number of lines of code in the method body
    private int numCodeLines = 0;

    //Parent class for this method
    private ClassInstance classParent = null;

    //Flag to indicate the method is declared by the parent class
    //As appose to being implemented as a abstract method as part of a object instance within a method
    private boolean isDeclared = true;

    //Create the method with a method name
    public MethodInstance(String instanceName) {
        super(instanceName);
    }

    //Create the method instance with a name and parent class instance
    public MethodInstance(String instanceName, ClassInstance parent) {
        super(instanceName);
    }

    //Override the stored number of code branches
    public void setLogicalOperations(int operations){
        logicalOperations = operations;
    }

    //Get the number of code branches or complexity of the method
    public int getComplexity() {
        return logicalOperations;
    }


    //Override the stored parent class reference
    public void setParent(ClassInstance parent){
        classParent = parent;
    }


    //Increment the number of code paths in the method by one
    public void addLogicalOperation(){
        ++logicalOperations;
    }

    //Set the number of method parameters
    public void setNumArguments(int args){ numArguments = args;}

    //Increment the number of arguments for this method
    public void incrementArgument(){
        ++numArguments;
    }

    //Get the number of arguments for this method
    public int getNumArguments(){
        return numArguments;
    }

    //Get the number of code lines in the method body
    public int getNumLines(){
        return numCodeLines;
    }

    //Set the number of liens of code in the method body
    public void setNumLines(int numLines){
        numCodeLines = numLines;
    }

    //Update whether the method is declared by the parent class or not
    public void setDeclared(boolean declared){
        isDeclared = declared;
    }

    //Check if method is declared by the parent class
    public boolean isDeclared() {
        return isDeclared;
    }

    //Print out the method name and other stored data
    @Override
    public String toString() {
        return "Method: " + getName() + ", Parent: " + classParent.getName() + ", Modifiers: " + getModifierString() +
                ", Logical Operations: " + logicalOperations + ", Parameters: " + numArguments;
    }
}
