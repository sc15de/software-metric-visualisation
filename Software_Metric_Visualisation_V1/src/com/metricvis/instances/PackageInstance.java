package com.metricvis.instances;

import java.util.ArrayList;

//Representation of a package or component containing a collection of classes
public class PackageInstance extends InstanceBase {

    //List of classes belonging to the package
    private ArrayList<ClassInstance> classes = new ArrayList<ClassInstance>();

    //Create package with a name
    public PackageInstance(String packageName){
        super(packageName);
    }


    //Add a class to the list of contained classes
    public void addClass(ClassInstance newClass){
        if (!classes.contains(newClass)){
            classes.add(newClass);
        }
    }


    //Get all contained classes
    public ArrayList<ClassInstance> getClassList(){
        return classes;
    }


    //Return a detailed description of the package showing the contained classes and methods
    public String toFullString(){
        String fullString = toString();
        for (ClassInstance classInstance : classes){
            fullString += "\n  " + classInstance.toString();
            for (MethodInstance method : classInstance.getMethods()){
                fullString += "\n    " + method.toString();
            }
        }
        return fullString;
    }

    //Print the held package data
    @Override
    public String toString() {
        return "Package: " + getName() + ", Contained Classes: " + classes.size();
    }


}
