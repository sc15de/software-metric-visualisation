package com.metricvis.instances;

import java.util.ArrayList;


//Representation of a class component containing held methods, attributes and known dependencies
public class ClassInstance extends InstanceBase {

    //List of method instances contained by the class
    private ArrayList<MethodInstance> methods =  new ArrayList<MethodInstance>();
    //Names of all classes this class is dependant on
    //Used to build the list of outgoing and incoming dependencies
    private ArrayList<String> dependencyNames = new ArrayList<String>();
    //References to classes dependant on this class
    private ArrayList<ClassInstance> outDependencies =  new ArrayList<ClassInstance>();
    private ArrayList<Integer> outDependencyOccurances = new ArrayList<Integer>();
    //References to classes this class is dependant on
    private ArrayList<ClassInstance> inDependencies =  new ArrayList<ClassInstance>();
    private ArrayList<Integer> inDependencyOccurances = new ArrayList<Integer>();
    //Package containing this class, null if there is no parent package
    PackageInstance parentPackage = null;

    //Create the class instance with a name
    public ClassInstance(String name){
        super(name);
    }

    //Add an incoming dependency to the list
    public void addIncomingDependency(ClassInstance dependency){
        if (!inDependencies.contains(dependency)){
            inDependencies.add(dependency);
            inDependencyOccurances.add(new Integer(1));
        }else{
            Integer dependencyCount = inDependencyOccurances.get(inDependencies.indexOf(dependency));
            dependencyCount +=1;
        }
    }

    //Add a outgoing dependency to the list
    public void addOutgoingDependency(ClassInstance dependency){
        if (!outDependencies.contains(dependency)){
            outDependencies.add(dependency);
            outDependencyOccurances.add(new Integer(1));
        }else{
            Integer dependencyCount = outDependencyOccurances.get(outDependencies.indexOf(dependency));
            dependencyCount +=1;
        }
    }


    public ArrayList<Integer> getOutDependencyOccurances(){ return outDependencyOccurances; }

    public ArrayList<Integer> getInDependencyOccurances(){
        return inDependencyOccurances;
    }


    //Get the list of incoming dependencies
    public ArrayList<ClassInstance> getIncomingDependencies(){
        return inDependencies;
    }

    //Get the list of outgoing dependencies
    public ArrayList<ClassInstance> getOutgoingDependencies(){
        return outDependencies;
    }

    //Set the parent package for this class
    public void setParentPackage(PackageInstance parent){parentPackage = parent;}

    //Get the parent package for this class
    public PackageInstance getParentPackage(){return parentPackage;}

    //Add a reference to a method contained within the body of this class
    public void addMethod(MethodInstance method){
        if (!methods.contains(method)){
            method.setParent(this);
            methods.add(method);
        }
    }

    //Get the list of contained methods
    public ArrayList<MethodInstance> getMethods(){
        return methods;
    }

    //Add a name of a incoming dependency
    public void addDependencyName(String dependencyName){
        if (!dependencyName.equals(getName())) {
            dependencyNames.add(dependencyName);
        }
    }

    //Get list of known incoming dependency names
    public ArrayList<String> getDependencyNames(){
        return dependencyNames;
    }

    //Get the method at the given index position
    public MethodInstance getMethod(int i){
        return methods.get(i);
    }

    //Get the number of methods contained within this class
    public int getNumMethods(){
        return methods.size();
    }

    //Print the data stored in the class representations along with all contains methods
    public String toFullString(){
        if (methods.size() == 0){ return this.toString();}

        String fullClassString = this.toString() + "\n";

        fullClassString += "  Methods:\n";
        for (int i=0; i < methods.size(); ++i){
            fullClassString += "    " + methods.get(i);
            if (i != methods.size()-1){ fullClassString+="\n"; }
        }

        fullClassString += "\n  Outgoing Dependencies:\n";
        for (int i=0; i < outDependencies.size(); ++i){
            fullClassString += "    " + outDependencies.get(i).getName();
            if (i != outDependencies.size()-1){ fullClassString+="\n"; }
        }

        fullClassString += "\n  Incomming Dependencies:\n";
        for (int i=0; i < inDependencies.size(); ++i){
            fullClassString += "    " + inDependencies.get(i).getName();
            if (i != inDependencies.size()-1){ fullClassString+="\n"; }
        }

        return fullClassString;
    }


    //Find a contained method by method name
    public MethodInstance getMethod(String methodName){
        for (MethodInstance method : methods){
            if (method.getName().equals(methodName)){
                return method;
            }
        }
        return null;
    }


    //Remove any undeclared methods from the methods list
    public void removeUndeclaredMethods(){
        ArrayList<MethodInstance> newMethods = new ArrayList<MethodInstance>();
        for (MethodInstance mthd : methods){
            if (mthd.isDeclared()){
                newMethods.add(mthd);
            }
        }
        methods = newMethods;
    }

    //Output the class representation as a string presenting all held data
    @Override
    public String toString() {
        getModifierString();
        return "Class: " + getName() + ", Modifiers: " + getModifierString() + ", Methods: " +
                methods.size() + ", Outgoing Dependencies: " + outDependencies.size() + ", Incomming Dependencies: " + inDependencies.size();
    }
}
