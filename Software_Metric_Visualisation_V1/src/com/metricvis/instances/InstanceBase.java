package com.metricvis.instances;

import java.lang.reflect.Array;
import java.util.ArrayList;


//Base class for component representations
public abstract class InstanceBase {

    //Name of the component
    private String instanceName;
    //Attributes of the component e.g. access modifier or abstraction flags
    private ArrayList<String> modifiers = new ArrayList<String>();

    //Create instance with a component name
    public InstanceBase(String name){
        instanceName = name;
    }

    //Get the name of the component
    public String getName(){
        return instanceName;
    }

    //Each representation should print its name and associated features
    public abstract String toString();

    //Get a string containing each modifier for the component
    //Mainly used for debug
    protected String getModifierString(){
        String modifierString = "";

        if (modifiers.size() == 0){
            return "NONE";
        }

        for (int i=0; i < modifiers.size(); ++i){
            modifierString += modifiers.get(i);
            if (i != modifiers.size()-1){modifierString += ", ";}
        }
        return modifierString;
    }


    //Get the list of modifiers for the component
    public ArrayList<String> getModifiers(){
        return modifiers;
    }

    //Add a component modifier
    public void addModifier(String newModifier){
        modifiers.add(newModifier);
    }

}

