package com.metricvis.softwareversions;

import com.metricvis.instances.ClassInstance;
import com.metricvis.instances.MethodInstance;
import com.metricvis.instances.PackageInstance;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;



//Static class used as a utility for saving and opening software visualisation project files (.smf)
public class ProjectFileHandler {

    //Use the string values of the class dependency names to create a list of outgoing and incoming dependencies for each class
    private static ArrayList<ClassInstance> buildDependencies(ArrayList<ClassInstance> classList){
        for (int i=0; i< classList.size(); ++i){
            ClassInstance currentClass = classList.get(i);
            //Get the name of each class dependency
            ArrayList<String> dependencyNames = currentClass.getDependencyNames();

            //Find the class with the matching name
            for (String dependency : currentClass.getDependencyNames()){
                for (int j=0; j < classList.size(); ++j){
                    if (classList.get(j).getName().equals(dependency)){
                        //Add to current class as a incoming dependency
                        currentClass.addOutgoingDependency(classList.get(j));
                        //Add to other class as outgoing dependency
                        classList.get(j).addIncomingDependency(currentClass);
                        break;
                    }
                }
            }
        }
        return classList;
    }


    //Read a method from the project file under the given class instance
    private static MethodInstance readMethod(Scanner projectFileScanner, ClassInstance newClass){
        String[] methodAttr = projectFileScanner.nextLine().split(",");
        MethodInstance newMethod = new MethodInstance(methodAttr[0], newClass);

        //Get modifiers e.g. public static
        int methodModifiers = Integer.parseInt(methodAttr[1]);
        for (int k=2; k < methodModifiers; k+=1){ newMethod.addModifier(methodAttr[k]); };

        newMethod.setNumLines(Integer.parseInt(methodAttr[2+methodModifiers]));
        newMethod.setNumArguments(Integer.parseInt(methodAttr[3+methodModifiers]));
        newMethod.setLogicalOperations(Integer.parseInt(methodAttr[4+methodModifiers]));

        return newMethod;
    }


    //Read a class from the project file and add it to the relevant package
    private static ClassInstance readClass(Scanner projectFileScanner, ArrayList<PackageInstance> newPackages){
        String[] classAttr = projectFileScanner.nextLine().split(",");
        ClassInstance newClass = new ClassInstance(classAttr[0]);

        //Get modifiers e.g. public
        int classModifiers = Integer.parseInt(classAttr[2]);
        for (int j=3; j < classModifiers; j+=1){ newClass.addModifier(classAttr[j]); };

        //Read class out-going dependencies
        for (int j=3+classModifiers; j < classAttr.length-1; j+=1){
            newClass.addDependencyName(classAttr[j]);
        }


        //Construct methods
        int numMethods = Integer.parseInt(classAttr[classAttr.length-1]);
        for(int j=0; j < numMethods; ++j){
            newClass.addMethod(readMethod(projectFileScanner,newClass));
        }

        //Add this class to its parent package
        boolean isNewPackage = true;
        for (PackageInstance savedPackage : newPackages){
            if (savedPackage.getName().equals(classAttr[1])){
                newClass.setParentPackage(savedPackage);
                savedPackage.addClass(newClass);
                isNewPackage= false;
                break;
            }
        }

        //Create the package if it does not exist
        if (isNewPackage){
            PackageInstance parentPackage = new PackageInstance(classAttr[1]);
            newPackages.add(parentPackage);
            newClass.setParentPackage(parentPackage);
            parentPackage.addClass(newClass);
        }

        return newClass;
    }



    //Reads a milestone and all contains classes, packages and methods from a software visualisation file (.smf)
    private static Milestone readMilestone(Scanner projectFileScanner){
        Milestone newMilestone = null;

        String[] milestoneAttr = projectFileScanner.nextLine().split(",");

        //Read the milestone date
        try {
            Date milestoneDate = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(milestoneAttr[1]);
            newMilestone = new Milestone(milestoneDate);
        }catch(ParseException e){
            System.out.println("Failed to parse milestone date");
            e.printStackTrace();
            return null;
        }

        ArrayList<PackageInstance> newPackages = new ArrayList<PackageInstance>();
        ArrayList<ClassInstance> newClasses = new ArrayList<ClassInstance>();

        //Build classes and packages
        for (int i=0; i < Integer.parseInt(milestoneAttr[2]); ++i){
            newClasses.add(readClass(projectFileScanner, newPackages));
        }

        //Build class dependencies using the read list of outgoing dependency names
        newClasses = buildDependencies(newClasses);

        newMilestone.setClasses(newClasses);
        newMilestone.setPackages(newPackages);

        return newMilestone;
    }



    //Reads a software metric visualisation file (.smf) and generates a milestone list
    public static ArrayList<Milestone> readFile(String projectFilePath){
        ArrayList<Milestone> projectMilestones = new ArrayList<Milestone>();

        File projectFile = new File(projectFilePath);
        try{
            Scanner projectFileScanner = new Scanner(projectFile);

            //Build the milestones
            while (projectFileScanner.hasNextLine()){
                projectMilestones.add(readMilestone(projectFileScanner));
            }

        }catch(FileNotFoundException e){
            System.out.println("Failed to open project file: " + projectFilePath);
            e.printStackTrace();
        }

        return projectMilestones;
    }



    //Save the current software component and milestone representation as a software metric visualisation file (.smf)
    public static void exportProjectFile(ArrayList<Milestone> milestones, String path){
        File newProjectFile = new File(path);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
        try{
            if (!newProjectFile.createNewFile()){
                throw new IOException();
            }

            FileWriter projectWriter = new FileWriter(newProjectFile);

            //Write each milestone
            for (Milestone milestone : milestones){
                Date date = milestone.getDate();
                projectWriter.write("Milestone," + dateFormat.format(date) + "," + milestone.getClasses().size() + "\n");

                ArrayList<ClassInstance> milestoneClasses = milestone.getClasses();

                //Write each class in the milestone
                for (ClassInstance classInstance : milestoneClasses){
                    projectWriter.write(classInstance.getName() + ",");
                    projectWriter.write(classInstance.getParentPackage().getName() + ",");
                    projectWriter.write(classInstance.getModifiers().size() + ",");
                    for (String modifier : classInstance.getModifiers()){
                        projectWriter.write(modifier + ",");
                    }

                    ArrayList<String> dependencyNames = classInstance.getDependencyNames();
                    for (String dependencyName : dependencyNames){
                        projectWriter.write(dependencyName + ",");
                    }

                    projectWriter.write(classInstance.getNumMethods() + "\n");

                    //Write each method
                    for (MethodInstance method : classInstance.getMethods()){
                        projectWriter.write(method.getName() + "," + method.getModifiers().size() + ",");

                        for (String modifier : method.getModifiers()){
                            projectWriter.write(modifier + ",");
                        }

                        projectWriter.write(method.getNumLines() + ",");
                        projectWriter.write(method.getNumArguments() + "," + method.getComplexity() + "\n");
                    }

                }
            }

            projectWriter.close();

        }catch (IOException e){
            System.out.println("Failed to create file: " + path);
            e.printStackTrace();
            return;
        }
    }
}
