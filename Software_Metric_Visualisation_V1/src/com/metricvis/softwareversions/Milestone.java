package com.metricvis.softwareversions;

import com.metricvis.instances.ClassInstance;
import com.metricvis.instances.PackageInstance;
import java.util.ArrayList;
import java.util.Date;

//Representation of a software milestone
//Contains all classes and packages of the represented software version
public class Milestone {
    //List of packages in milestone
    private ArrayList<PackageInstance> milestonePackages = new ArrayList<PackageInstance>();
    //List of classes in milestone
    private ArrayList<ClassInstance> milestoneClasses = new ArrayList<ClassInstance>();
    //Date milestone was loaded
    private Date milestoneDate = new Date();

    //Create the milestone with a date of creation
    public Milestone(Date date){
        milestoneDate = date;
    }

    //Populate the milestone with classes
    public void setClasses(ArrayList<ClassInstance> classes){
        milestoneClasses = classes;
    }

    //Populate the milestone with packages
    public void setPackages(ArrayList<PackageInstance> classes){
        milestonePackages = classes;
    }

    //Get classes in milestone
    public ArrayList<ClassInstance> getClasses(){ return milestoneClasses; }

    //Get packages in milestone
    public ArrayList<PackageInstance> getPackages(){ return milestonePackages; }

    //Get the creation date of the milestone
    public Date getDate(){ return milestoneDate; }
}
