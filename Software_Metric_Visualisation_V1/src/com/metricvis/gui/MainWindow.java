package com.metricvis.gui;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import com.metricvis.Rendering.Camera;
import com.metricvis.Rendering.CanvasRenderer;
import com.metricvis.instances.ClassInstance;
import com.metricvis.metrics.ClassMetric;
import com.metricvis.metrics.DependencyMetric;
import com.metricvis.metrics.MetricCalculator;
import com.metricvis.parsingtools.Java8.JavaParserController;
import com.metricvis.softwareversions.Milestone;
import com.metricvis.softwareversions.ProjectFileHandler;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


//Main window class used to house the 3D visualisation and associated widgets
public class MainWindow extends JFrame implements ActionListener, MouseWheelListener {

    //Create parser controller object to parse the given JAVA source code file
    JavaParserController parser = new JavaParserController();

    //List of milestones included with the loaded project
    ArrayList<Milestone> projectMilestones = new ArrayList<Milestone>();

    //Panel containing all content in the window
    private static final JPanel window = new JPanel();
    //Panel containing all sphere metric and and milestone controls
    private static final JPanel uiArea = new JPanel();

    //Top menu bar for the window
    private static final JMenuBar menuBar = new JMenuBar();

    //Metric selection field for sphere size
    private static final JLabel ballSizeLabel = new JLabel("Ball Size", JLabel.LEFT);
    private static final JComboBox<String> ballSizeMetric = new JComboBox<String>();

    //Metric selection field for sphere colour
    private static final JLabel ballColourLabel = new JLabel("Ball Colour", JLabel.LEFT);
    private static final JComboBox<String> ballColourMetric = new JComboBox<String>();

    //Metric selection field for sphere link colour
    private static final JLabel linkColourLabel = new JLabel("Link Colour", JLabel.LEFT);
    private static final JComboBox<String> linkColourMetric = new JComboBox<String>();

    //Spinner and model used to display and select the desired milestone
    private JLabel milestoneNumberLabel = new JLabel("Milestone Number", JLabel.LEFT);
    private SpinnerNumberModel milestoneModel = new SpinnerNumberModel(1,1,1,1);
    private JSpinner milestoneNumber = new JSpinner(milestoneModel);

    //Date field used to display the date and time of creation for the currently displayed milestone
    private static final JLabel dateLabel = new JLabel("Date", JLabel.LEFT);
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
    private static final JTextField milestoneDate = new JTextField(dateFormat.format(new Date()));

    //Checkbox used to enable or disable the drawing of size guides around each sphere in the visualisation
    private static final JCheckBox showGuides = new JCheckBox("Show Guides");

    //Slider used to select the desired milestone
    private static final JSlider mileStoneSlider = new JSlider(JSlider.HORIZONTAL);

    //Play and pause image and label. Used to start or stop slideshow of software milestones
    private static final ImageIcon playImage = new ImageIcon(new ImageIcon("images/playImage.png")
            .getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));
    private static final ImageIcon pauseImage = new ImageIcon(new ImageIcon("images/pauseImage.png")
            .getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));

    private static final JLabel playImageLabel = new JLabel(playImage);
    private static final JLabel pauseImageLabel = new JLabel(pauseImage);

    //OpenGL render area components
    private static GLCanvas renderArea;
    private static final CanvasRenderer mainRender = new CanvasRenderer();
    private static final JPanel canvasContainer = new JPanel();

    private static ImageIcon legendImage = new ImageIcon(new ImageIcon("images/ColourLegend.png")
            .getImage().getScaledInstance(200, 750, Image.SCALE_DEFAULT));

    private static final ImageIcon largeLegendImage = new ImageIcon(new ImageIcon("images/Icon.png")
            .getImage().getScaledInstance(50, 50, Image.SCALE_DEFAULT));

    private static final ImageIcon mediumLegendImage = new ImageIcon(new ImageIcon("images/Icon.png")
            .getImage().getScaledInstance(35, 35, Image.SCALE_DEFAULT));

    private static final ImageIcon smallLegendImage = new ImageIcon(new ImageIcon("images/Icon.png")
            .getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));

    private static final JLabel legendImageLabel = new JLabel(legendImage);
    private static final JLabel legendLabel = new JLabel("Legend");
    private static final JLabel lowLabel = new JLabel("Too Low");
    private static final JLabel idealLabel = new JLabel("Ideal");
    private static final JLabel highLabel = new JLabel("Too High");
    private static final JLabel smallLegendLabel = new JLabel(smallLegendImage);
    private static final JLabel mediumLegendLabel = new JLabel(mediumLegendImage);
    private static final JLabel largeLegendLabel = new JLabel(largeLegendImage);


    private static final JButton resetButton = new JButton("Reset View");

    //Currently selected and displayed milestone
    private int currentMilestone = 0;

    //Timer event to move to the next milestone after one second
    //Implements slideshow of milestone visualisations
    Timer milestoneShowTimer = new Timer(1000, new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (currentMilestone != projectMilestones.size() -1){
                setMilestone(currentMilestone+1);
            }
        }
    });


    //Add a milestone to the current project
    private void addMilestone(Milestone newMilestone){
        projectMilestones.add(newMilestone);
        mileStoneSlider.setMaximum(projectMilestones.size()-1);
        milestoneModel.setMaximum(projectMilestones.size());
        mainRender.createSpheres(projectMilestones);
        mainRender.setMilestone(0);
        setMilestone(0);
    }


    //Build the OpenGL render area and its containers
    private void buildCanvas(){
        GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);
        renderArea = new GLCanvas(capabilities);

        renderArea.addGLEventListener(mainRender);

        canvasContainer.add(renderArea);

        int renderWith = (getWidth() / 3) *2;
        int renderHeight = getHeight() - 70;

        renderArea.setSize(renderWith, renderHeight);
        canvasContainer.setSize(renderWith,renderHeight);
        canvasContainer.setLocation(0,-5);

        projectMilestones.addAll(ProjectFileHandler.readFile("samples/Inheritance Example.smf"));
        mainRender.createSpheres(projectMilestones);
        mileStoneSlider.setMaximum(projectMilestones.size()-1);
        milestoneModel.setMaximum(projectMilestones.size());
        setMilestone(0);

        //Create animator to re-draw visualisation at 30fps
        final FPSAnimator animator = new FPSAnimator(renderArea, 30,false);
        animator.start();

        window.add(canvasContainer);

    }


    //Build the menu bar widget populating it with menu options
    private void buildMenuBar(){
        Font menuFont = new Font("sans-serif", Font.PLAIN, 16);

        UIManager.put("Menu.font", menuFont);
        UIManager.put("MenuItem.font", menuFont);

        menuBar.setPreferredSize(new Dimension(getWidth(),35));

        //Button to clear current project
        JMenuItem newProjectButton = new JMenuItem("New Project");
        newProjectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                projectMilestones.clear();
                mainRender.createSpheres(projectMilestones);
                mileStoneSlider.setMaximum(0);
                milestoneModel.setMaximum(1);
                setMilestone(0);
            }
        });
        menuBar.add(newProjectButton);


        //Button to open a smf project file and display the contains milestone data
        JMenuItem openProjectButton = new JMenuItem("Open Project File");
        openProjectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                fileDialog.setDialogTitle("Open Project File");
                fileDialog.setSize(600,600);
                fileDialog.setCurrentDirectory(new File("samples"));
                fileDialog.addChoosableFileFilter(new FileNameExtensionFilter("Software Metric File",".smf"));
                int option = fileDialog.showOpenDialog(window);
                if (option == JFileChooser.APPROVE_OPTION) {

                    String filePath = fileDialog.getSelectedFile().getAbsolutePath();
                    System.out.println(filePath);

                    projectMilestones.clear();
                    projectMilestones.addAll(ProjectFileHandler.readFile(filePath));
                    mainRender.createSpheres(projectMilestones);
                    mileStoneSlider.setMaximum(projectMilestones.size() - 1);
                    milestoneModel.setMaximum(projectMilestones.size());
                    setMilestone(0);
                    mainRender.resetCamera();
                }
            }
        });
        menuBar.add(openProjectButton);


        //Button to add a Java milestone to the current project
        JMenuItem addMilestoneButton = new JMenuItem("Add Java Milestone");
        addMilestoneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setDialogTitle("Add Java Milestone");
                fileDialog.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                fileDialog.setSize(600,600);
                fileDialog.setCurrentDirectory(new File("samples"));
                int option = fileDialog.showOpenDialog(window);
                if (option == JFileChooser.APPROVE_OPTION) {
                    String filePath = fileDialog.getSelectedFile().getAbsolutePath();
                    System.out.println(filePath);

                    addMilestone(parser.runParser(filePath));
                    mainRender.resetCamera();
                }
            }

        });
        menuBar.add(addMilestoneButton);

        //Button to save all recorded milestones and software components to a smf file
        JMenuItem exportProjectButton = new JMenuItem("Export Project");
        exportProjectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileDialog = new JFileChooser();
                fileDialog.setDialogTitle("Export Project");
                fileDialog.setSize(600,600);
                fileDialog.setCurrentDirectory(new File("samples"));
                int option = fileDialog.showSaveDialog(window);
                if (option == JFileChooser.APPROVE_OPTION) {
                    String filePath = fileDialog.getSelectedFile().getAbsolutePath() + ".smf";
                    System.out.println(filePath);

                    ProjectFileHandler.exportProjectFile(projectMilestones, filePath);
                }
            }
        });
        menuBar.add(exportProjectButton);

        setJMenuBar(menuBar);
    }


    //Sets the currently displayed milestone and updates visualisation render
    private void setMilestone(int milestoneIndex){
        if (milestoneIndex == currentMilestone){return;}
        if (projectMilestones.size() == 0){
            mileStoneSlider.setValue(0);
            milestoneNumber.setValue(1);
            milestoneDate.setText("No Milestone");
            currentMilestone = 0;
            return;
        }

        if (milestoneIndex >= projectMilestones.size()){setMilestone(projectMilestones.size()-1);}
        if (milestoneIndex == projectMilestones.size()-1){ milestoneShowTimer.stop();}

        currentMilestone = milestoneIndex;
        mileStoneSlider.setValue(milestoneIndex);
        milestoneNumber.setValue(milestoneIndex+1);
        milestoneDate.setText(dateFormat.format(projectMilestones.get(milestoneIndex).getDate()));
        mainRender.setMilestone(milestoneIndex);
    }


    //Builds the visualisation control widgets on the window
    private void buildUI(){
        Font uiFontHeading = new Font("Arial", Font.BOLD, 18);
        Font uiFontBold = new Font("Arial", Font.BOLD, 16);
        Font uiFont = new Font("Arial", Font.PLAIN, 16);
        ballSizeLabel.setFont(uiFontBold);
        ballSizeMetric.setFont(uiFont);
        ballColourLabel.setFont(uiFontBold);
        ballColourMetric.setFont(uiFont);
        linkColourLabel.setFont(uiFontBold);
        linkColourMetric.setFont(uiFont);
        dateLabel.setFont(uiFontBold);
        legendLabel.setFont(uiFontHeading);
        idealLabel.setFont(uiFontBold);
        lowLabel.setFont(uiFontBold);
        highLabel.setFont(uiFontBold);
        milestoneNumberLabel.setFont(uiFontBold);
        showGuides.setFont(uiFontBold);
        resetButton.setFont(uiFontBold);


        //Create gridBag layout for the control widgets
        GridBagLayout uiLayout = new GridBagLayout();
        GridBagConstraints uiLayoutConstraints = new GridBagConstraints();

        uiArea.setLayout(uiLayout);

        mileStoneSlider.setPaintTicks(true);
        mileStoneSlider.setPaintTrack(true);
        mileStoneSlider.setPaintLabels(true);
        mileStoneSlider.setSnapToTicks(false);

        //List of selectable per-class metrics to modify sphere colour and size
        String[] viewableMetrics = {
                MetricCalculator.NoClassMetric.getName(),
                MetricCalculator.ClassInstability.getName(),
                MetricCalculator.NumberOfMethods.getName(),
                MetricCalculator.AverageMethodComplexity.getName(),
                MetricCalculator.HighestMethodComplexity.getName(),
                MetricCalculator.AverageMethodParameters.getName(),
                MetricCalculator.HighestMethodParameters.getName(),
                MetricCalculator.AverageMethodSize.getName(),
                MetricCalculator.HighestMethodSize.getName()
        };

        //Add class metrics to ball size selection combo box
        ballSizeMetric.addActionListener(this);
        for (String metric : viewableMetrics){
            ballSizeMetric.addItem(metric);
        }
        ballSizeMetric.setSelectedIndex(0);


        //Add class metrics to ball colour selection combo box
        ballColourMetric.addActionListener(this);
        for (String metric : viewableMetrics){
            ballColourMetric.addItem(metric);
        }
        ballColourMetric.setSelectedIndex(0);

        //List of selectable per-dependency metrics for link colour
        String[] linkMetrics = {
                MetricCalculator.NoDependencyMetric.getName(),
                MetricCalculator.NumReferences.getName(),
                MetricCalculator.NumReferencesDeviation.getName(),
                MetricCalculator.isInCycle.getName(),
                MetricCalculator.isInImmediateCycle.getName()};

        //Add link metrics to link colour selection combo box
        linkColourMetric.addActionListener(this);
        for (String metric : linkMetrics){
            linkColourMetric.addItem(metric);
        }

        //Setup spinner model text and add action to change milestone to match its value
        JSpinner.NumberEditor numberEditor = new JSpinner.NumberEditor(milestoneNumber);
        numberEditor.getTextField().setHorizontalAlignment(JTextField.CENTER);
        milestoneNumber.setEditor(numberEditor);
        milestoneModel.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                setMilestone((Integer)milestoneNumber.getValue()-1);
            }
        });


        showGuides.addActionListener(this);


        //Play label or image which starts the milestone slideshow when clicked
        playImageLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (projectMilestones.size() < 2){return;}
                if (currentMilestone == projectMilestones.size()-1){
                    setMilestone(0);
                }
                milestoneShowTimer.setRepeats(true);
                milestoneShowTimer.restart();
            }
        });


        //Pause label or image which stops the milestone slideshow when clicked
        pauseImageLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                milestoneShowTimer.stop();
            }
        });

        //Setup milestone date field
        milestoneDate.setHorizontalAlignment(JTextField.CENTER);
        milestoneDate.setFont(uiFont);
        milestoneDate.setEditable(false);

        //Setup milestone slider with initial value range and update represented milestone when changed
        mileStoneSlider.setMinimum(0);
        mileStoneSlider.setMaximum(0);
        mileStoneSlider.setValue(0);
        mileStoneSlider.setPaintTicks(true);
        mileStoneSlider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                setMilestone(mileStoneSlider.getValue());
            }
        });


        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.resetCamera();
            }
        });

        window.add(lowLabel);
        window.add(highLabel);
        window.add(idealLabel);
        window.add(legendLabel);
        window.add(largeLegendLabel);
        window.add(mediumLegendLabel);
        window.add(smallLegendLabel);


        //Setup locations of each widget in the grid bag layoug
        uiLayoutConstraints.weightx = 1.0f;
        uiLayoutConstraints.weighty = 1.0f;


        //Set ball size field location
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 1;
        uiArea.add(ballSizeLabel,uiLayoutConstraints);
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 2;
        uiArea.add(ballSizeMetric,uiLayoutConstraints);

        //Set ball colour field location
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 3;
        uiArea.add(ballColourLabel,uiLayoutConstraints);
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 4;
        uiArea.add(ballColourMetric,uiLayoutConstraints);

        //Set link colour field location
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 5;
        uiArea.add(linkColourLabel,uiLayoutConstraints);
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 6;
        uiArea.add(linkColourMetric,uiLayoutConstraints);

        //Set milestone spinner field location
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 7;
        uiArea.add(milestoneNumberLabel,uiLayoutConstraints);
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 8;
        uiArea.add(milestoneNumber,uiLayoutConstraints);

        //Set milestone date field location
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 9;
        uiArea.add(dateLabel,uiLayoutConstraints);
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 10;
        uiArea.add(milestoneDate,uiLayoutConstraints);

        //Set show guides checkbox location
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 11;
        uiArea.add(showGuides,uiLayoutConstraints);

        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 3;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 12;
        uiArea.add(resetButton,uiLayoutConstraints);

        //Set slideshow control locations
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 1;
        uiLayoutConstraints.gridx = 2;
        uiLayoutConstraints.gridy = 13;
        uiArea.add(playImageLabel,uiLayoutConstraints);
        uiLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        uiLayoutConstraints.gridwidth = 1;
        uiLayoutConstraints.gridx = 3;
        uiLayoutConstraints.gridy = 13;
        uiArea.add(pauseImageLabel,uiLayoutConstraints);

        window.add(uiArea);
        window.add(mileStoneSlider);
        window.add(legendImageLabel);
    }



    //Setup key bindings for the camera scene transformations
    private void setupActions(){
        Camera mainCamera = mainRender.mainCamera;

        //Map camera movement controls when key is pressed
        window.getActionMap().put("Forward", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.addMovement(Camera.MOVE_DIRECTION.FORWARD);
            }});
        window.getActionMap().put("Backward", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.addMovement(Camera.MOVE_DIRECTION.BACKWARD);
            }});

        window.getActionMap().put("Left", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.addMovement(Camera.MOVE_DIRECTION.LEFT);
            }});

        window.getActionMap().put("Right", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.addMovement(Camera.MOVE_DIRECTION.RIGHT);
            }});
        window.getActionMap().put("Up", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.addMovement(Camera.MOVE_DIRECTION.UP);
            }});
        window.getActionMap().put("Down", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.addMovement(Camera.MOVE_DIRECTION.DOWN);
            }});


        //Map camera movement controls when key is released
        window.getActionMap().put("ForwardRelease", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.removeMovement(Camera.MOVE_DIRECTION.FORWARD);
            }});
        window.getActionMap().put("BackwardRelease", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.removeMovement(Camera.MOVE_DIRECTION.BACKWARD);
            }});

        window.getActionMap().put("LeftRelease", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.removeMovement(Camera.MOVE_DIRECTION.LEFT);
            }});

        window.getActionMap().put("RightRelease", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.removeMovement(Camera.MOVE_DIRECTION.RIGHT);
            }});
        window.getActionMap().put("UpRelease", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.removeMovement(Camera.MOVE_DIRECTION.UP);
            }});
        window.getActionMap().put("DownRelease", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.removeMovement(Camera.MOVE_DIRECTION.DOWN);
            }});


        //Set camera yaw rotation controls
        window.getActionMap().put("RotateLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSceneRotationYaw(-1);
            }});

        window.getActionMap().put("RotateRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSceneRotationYaw(1);
            }});

        window.getActionMap().put("ReleaseRotationYaw", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSceneRotationYaw(0);
            }});


        //Set camera pitch rotation controls
        window.getActionMap().put("RotateUp", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSceneRotationPitch(-1);
            }});

        window.getActionMap().put("RotateDown", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSceneRotationPitch(1);
            }});

        window.getActionMap().put("ReleaseRotationPitch", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSceneRotationPitch(0);
            }});

        window.getActionMap().put("CameraSpeedup", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSpeed(mainRender.mainCamera.getSpeed() + 2f);
            }});

        window.getActionMap().put("CameraSlowdown", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mainRender.mainCamera.setSpeed(mainRender.mainCamera.getSpeed() - 2f);
            }});


        //Camera movement actions when key is pressed (adds associated to list of performed movements)
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_W,0, false), "Forward");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S,0, false), "Backward");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_A,0, false), "Left");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_D,0, false), "Right");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE,0, false), "Up");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_C,0, false), "Down");

        //Camera movement actions when key is released (used to stop movement in the associated direction)
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_W,0, true), "ForwardRelease");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S,0, true), "BackwardRelease");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_A,0, true), "LeftRelease");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_D,0, true), "RightRelease");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE,0, true), "UpRelease");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_C,0, true), "DownRelease");

        //Camera Yaw rotation actions
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,0, false), "RotateLeft");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT,0, true), "ReleaseRotationYaw");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT,0, false), "RotateRight");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT,0, true), "ReleaseRotationYaw");

        //Camera pitch rotation actions
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,0, false), "RotateUp");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_UP,0, true), "ReleaseRotationPitch");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,0, false), "RotateDown");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN,0, true), "ReleaseRotationPitch");


        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_PERIOD,0, true), "CameraSpeedup");
        window.getInputMap(window.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_COMMA,0, true), "CameraSlowdown");


    }


    //Configure the main window and build its contained components
    private void buildWindow(){
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        ImageIcon icon = new ImageIcon("images/icon.png");
        setSize((int)(screen.getWidth() /1.5f),(int)(screen.getHeight()/1.25f));

        setIconImage(icon.getImage());
        setLocation((int)(screen.getWidth()/2) - (getWidth()/2),(int)(screen.getHeight()/2) - (getHeight()/2));
        setTitle("Software Metric Visualisation");
        setLayout(null);
        window.setLayout(null);
        addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                onResize(e);
            }
        });
        window.addMouseWheelListener(this);
        setContentPane(window);

        setResizable(false);
        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                System.out.println("Closed");
                e.getWindow().dispose();
                System.exit(1);
            }
        });

        buildMenuBar();
        buildCanvas();
        buildUI();
        setupActions();
    }


    //Function called when the window is resized
    public void onResize(ComponentEvent e){

        //Resize render area
        int renderWith = (getWidth() / 3) *2;
        int renderHeight = getHeight() - 240;

        renderArea.setSize(renderWith, renderHeight);
        canvasContainer.setSize(renderWith,renderHeight);

        int uiWidth = (getWidth() / 3) -50;
        int uiHeight = getHeight() - 70;

        int uiX = canvasContainer.getX() + canvasContainer.getWidth() + 20;
        int uiY = -5;
        uiArea.setSize(uiWidth, uiHeight);
        uiArea.setLocation(uiX, uiY);
        uiArea.setSize(uiWidth, uiHeight);


        legendLabel.setSize(75,30);
        legendLabel.setLocation(10,uiY + canvasContainer.getHeight());

        legendImage = new ImageIcon(legendImage.getImage().getScaledInstance(renderWith-20, 50, Image.SCALE_DEFAULT));
        legendImageLabel.setIcon(legendImage);
        legendImageLabel.setSize(renderWith-20, 30);
        legendImageLabel.setLocation(10,legendLabel.getY()+30);

        lowLabel.setSize(75,25);
        lowLabel.setLocation(10+legendImageLabel.getX(), legendImageLabel.getY());

        idealLabel.setSize(75,25);
        idealLabel.setLocation(legendImageLabel.getX()+(legendImageLabel.getWidth() / 2)-50, legendImageLabel.getY());

        highLabel.setSize(75,25);
        highLabel.setLocation(legendImageLabel.getX() + legendImageLabel.getWidth()-75, legendImageLabel.getY());

        smallLegendLabel.setSize(50,50);
        smallLegendLabel.setLocation(lowLabel.getX(),lowLabel.getY() + 40);

        mediumLegendLabel.setSize(50,50);
        mediumLegendLabel.setLocation(idealLabel.getX()-10,idealLabel.getY() + 40);

        largeLegendLabel.setSize(50,50);
        largeLegendLabel.setLocation(highLabel.getX()+10,highLabel.getY() + 40);


        //Resize and move milestone slider
        mileStoneSlider.setSize(renderWith-20, 50);
        mileStoneSlider.setLocation(10,largeLegendLabel.getY() + largeLegendLabel.getHeight() +10);




        ballSizeMetric.setLocation(ballSizeLabel.getX(), ballSizeLabel.getY() + 25);
        ballColourMetric.setLocation(ballColourLabel.getX(), ballColourLabel.getY() + 25);
        linkColourMetric.setLocation(linkColourLabel.getX(), linkColourLabel.getY() + 25);
        milestoneNumber.setLocation(milestoneNumberLabel.getX(), milestoneNumberLabel.getY() + 25);
        milestoneDate.setLocation(dateLabel.getX(), dateLabel.getY() + 25);
    }


    //Construct and then show the window
    public void start(){
        buildWindow();
        setVisible(true);
    }


    //Action event for all combo boxes and show guide checkbox
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(ballSizeMetric)){
            String metricName = ballSizeMetric.getItemAt(ballSizeMetric.getSelectedIndex());
            //Find the selected class metric by name and update the visualisation
            for (ClassMetric metric : MetricCalculator.getClassMetrics()){
                if (metric.getName().equals(metricName)){
                    mainRender.setSizeMetric(metric);
                    break;
                }
            }
        }

        if (e.getSource().equals(ballColourMetric)){
            String metricName = ballColourMetric.getItemAt(ballColourMetric.getSelectedIndex());
            //Find the selected class metric by name and update the visualisation
            for (ClassMetric metric : MetricCalculator.getClassMetrics()){
                if (metric.getName().equals(metricName)){
                    mainRender.setColourMetric(metric);
                    break;
                }
            }

        }

        if (e.getSource().equals(linkColourMetric)){
            String metricName = linkColourMetric.getItemAt(linkColourMetric.getSelectedIndex());
            //Find the selected dependency metric by name and update the visualisation
            for (DependencyMetric metric : MetricCalculator.getDependencyMetrics()){
                if (metric.getName().equals(metricName)){
                    mainRender.setLinkMetric(metric);
                    break;
                }
            }
        }

        if (e.getSource().equals(showGuides)){
            //Update visualisation to show guide circles
            mainRender.showGuides(showGuides.isSelected());
        }

    }

    //Triggered when the mouse scroll wheel is moved
    //Increases or decrease the speed of the camera movement
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        mainRender.mainCamera.setSpeed(mainRender.mainCamera.getSpeed() + (1f*-e.getWheelRotation()));
    }
}




