package com.metricvis.Rendering;

import com.metricvis.instances.ClassInstance;

import java.util.ArrayList;
import java.util.Random;

import static com.metricvis.Rendering.CanvasRenderer.getMagnitude;

//Represents a cluster of spheres used to keep spheres representing classes with a high number
//of dependencies to each-other in close proximity
public class Cluster extends Model{

    //ID for this cluster
    int clusterID = -1;

    //Create cluster with a origin point as a reference spheres within the cluster
    Cluster(float x, float y, float z, int ID) {
        super(x, y, z);
        clusterID = ID;
    }

    //List of spheres that are contained within this cluster
    private ArrayList<ClassSphere> clusterSpheres = new ArrayList<ClassSphere>();

    //Get the list of spehres within this cluster
    public ArrayList<ClassSphere> getSpheres(){ return clusterSpheres; }

    //Get the number of spheres in this cluster
    public int getSize(){return clusterSpheres.size();}

    //Add a sphere to the cluster adding to the list of contained spheres and
    //calculating a valid position for it.
    public void addSphere(ClassSphere sphere){
        sphere.setCluster(clusterID);
        clusterSpheres.add(sphere);
    }

    //Get the spatial radius of the cluster
    //All spheres within the cluster will be placed within this radius from the cluster origin
    public float getClusterRadius(){
        return ((clusterSpheres.size() * clusterSpheres.get(0).getMaxScale()) / 2);
    }

    //Get the combined number of incomming and outgoing dependencies the given sphere has
    //with the spheres currently in the cluster
    //Ideal for determining if the given sphere should be placed in this cluster
    public int getLinks(ClassSphere sphere){

        int linksInCluster = 0;
        ClassInstance sphereClass = sphere.getRepresentedClass();
        ArrayList<ClassInstance> classDepencencies = new ArrayList<ClassInstance>();
        classDepencencies.addAll(sphereClass.getIncomingDependencies());
        classDepencencies.addAll(sphereClass.getOutgoingDependencies());

        for (ClassSphere clusterSphere : clusterSpheres){
            if (classDepencencies.contains(clusterSphere.getRepresentedClass())) {
                ++linksInCluster;
            }
        }
        //if (linksInCluster >= (classDepencencies.size() / 2)){ return true; }
        return linksInCluster;
    }



    //Place all spheres for the given milestone contained in the sphereList list
    public void placeSpheres(ArrayList<ArrayList<ClassSphere>> sphereList, int milestoneIndex){
        //Create and place each sphere in the scene
        for (ClassSphere newSphere : clusterSpheres){
            boolean isNewSphere = true;
            for (ArrayList<ClassSphere> milestoneSpheres : sphereList){
                for (ClassSphere sphere : milestoneSpheres) {
                    if (sphere.getName().equals(newSphere.getName())){
                        isNewSphere = false;
                        //Sphere with same name exists in previous milestone so copy its position
                        newSphere.setPosition(sphere.getPosX(), sphere.getPosY(), sphere.getPosZ());
                        break;
                    }
                }
                if (!isNewSphere){break;}
            }
            //First occurrence of class so create a new sphere and location for it
            if (isNewSphere){
                placeSphere(newSphere, sphereList);
            }
            sphereList.get(milestoneIndex).add(newSphere);
        }


    }


    //Places a sphere in the scene with a scaled minimum and maximum distance from the origin of the scene
    //Makes sure that no spheres are intersecting and that all spheres have line-of-sight to spheres that represent
    //a class dependency
    private void placeSphere(ClassSphere sphere, ArrayList<ArrayList<ClassSphere>> sphereList){

        if (clusterSpheres.size() == 1){sphere.setPosition(getPosX(), getPosY(), getPosZ());}
        ClassInstance sphereClass = sphere.getRepresentedClass();
        Random randomGenerator = new Random();

        //Create a list containing both incomming and outgoing dependencies
        ArrayList<ClassInstance> allDependencies = new ArrayList<ClassInstance>();
        allDependencies.addAll(sphereClass.getOutgoingDependencies());
        allDependencies.addAll(sphereClass.getIncomingDependencies());

        float minDistance = sphere.getMaxScale();
        float maxDistance = (getClusterRadius()*1.75f) + minDistance;
        float[] randomPosition = {0,0,0};

        boolean foundPosition = false;

        int maxChecks = 100;

            //Keep checking to find a final position
            while (!foundPosition){
                if (maxChecks <=0 ){
                    throw new RuntimeException("Could not find a valid position for a sphere!");
                }
                --maxChecks;

            //Find a random location within minimum and maximum bounds
            float distFromCenter = 0;
            while (distFromCenter < minDistance){

                randomPosition[0] = (randomGenerator.nextFloat() * maxDistance) - maxDistance/2;
                randomPosition[1] = (randomGenerator.nextFloat() * maxDistance) - maxDistance/2;
                randomPosition[2] = (randomGenerator.nextFloat() * maxDistance) - maxDistance/2;
                distFromCenter = getMagnitude(randomPosition);
            }

            sphere.setPosition(randomPosition[0] + getPosX(), randomPosition[1] + getPosY(), randomPosition[2] + getPosZ());

            //Check if sphere has line-of-sight of all its dependencies
            //Also checks for collided spheres
            foundPosition = true;
            for (ArrayList<ClassSphere> milestoneSpheres : sphereList) {
                for (ClassSphere otherSphere : milestoneSpheres) {
                    if (!allDependencies.contains(otherSphere.getRepresentedClass())) {
                        continue;
                    }
                    if (!isInSight(sphere, otherSphere, milestoneSpheres)) {
                        foundPosition = false;
                        break;
                    }
                }
                if (!foundPosition) {break;}
            }
        }
    }



    //Returns the distance of the given 3D point from the given sphere
    private static float getDistanceFromSphere(float[] pos, ClassSphere otherSphere){
        float[] localVec = {
                otherSphere.getPosX() - pos[0],
                otherSphere.getPosY() - pos[1],
                otherSphere.getPosZ() - pos[2]};

        return getMagnitude(localVec);
    }


    //Performs shape-casting to check if the destination sphere is in sight of the source sphere
    private boolean isInSight(ClassSphere source, ClassSphere destination, ArrayList<ClassSphere> spheres){
        float[] sourcePos = {source.getPosX(), source.getPosY(), source.getPosZ()};

        //Make sure source and destination do not intersect
        float distanceFromDestination = getDistanceFromSphere(sourcePos, destination);
        if (distanceFromDestination < source.getMaxScale()){ return false; }

        int numMarches = (int)(distanceFromDestination+1);

        float[] vec = {
                destination.getPosX() - source.getPosX(),
                destination.getPosY() - source.getPosY(),
                destination.getPosZ() - source.getPosZ()};

        float[] march = {source.getPosX(), source.getPosY(), source.getPosZ()};

        //March across a ray checking for intersections at each point on the ray
        for (int i=0; i < numMarches; ++i){

            for (ClassSphere otherSphere : spheres) {
                if (otherSphere.getName().equals(destination.getName())
                        || otherSphere.getName().equals(source.getName())) {
                    continue;
                }

                //Calculate minimum distance between spheres
                float minSphereDistance = source.getMaxScale() * 0.5f;
                if (i == 0){minSphereDistance = 3.5f;}

                if (getDistanceFromSphere(march, otherSphere) < minSphereDistance) {
                    return false;
                }

                //Move forward along shape ray
                march[0] += vec[0] / source.getMaxScale();
                march[1] += vec[1] / source.getMaxScale();
                march[2] += vec[2] / source.getMaxScale();
            }
        }


        return true;
    }

}
