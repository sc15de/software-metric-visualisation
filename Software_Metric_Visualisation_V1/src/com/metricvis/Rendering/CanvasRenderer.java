package com.metricvis.Rendering;


import com.jogamp.opengl.*;
import com.jogamp.opengl.glu.GLU;
import com.metricvis.instances.ClassInstance;
import com.metricvis.metrics.ClassMetric;
import java.util.ArrayList;
import java.util.Random;
import com.metricvis.metrics.DependencyMetric;
import com.metricvis.metrics.MetricCalculator;
import com.metricvis.softwareversions.Milestone;
import static java.lang.Math.*;
import static java.lang.StrictMath.pow;

//Renderer class which uses JOGL OpenGL bindings to draw a 3D scene on a GLCanvas
public class CanvasRenderer implements GLEventListener {

    //List of drawable spheres in each milestone
    private ArrayList<ArrayList<ClassSphere>> sphereList = new ArrayList<ArrayList<ClassSphere>>();
    private ArrayList<ArrayList<Cluster>> milestoneClusters = new ArrayList<ArrayList<Cluster>>();

    //List of drawable spheres to be drawn in the next frame
    private ArrayList<ClassSphere> drawList = new ArrayList<ClassSphere>();

    //Current metric represented by size on all spheres
    private ClassMetric sizeMetric = MetricCalculator.NoClassMetric;
    //Current metric represented by colour on all spheres
    private ClassMetric colourMetric = MetricCalculator.NoClassMetric;
    //Current metric represented by the colour of the links between spheres
    private DependencyMetric linkMetric = MetricCalculator.NoDependencyMetric;
    //Flag to show guide circles on all spheres
    private boolean drawGuides = false;

    //Currently selected milestone being displayed
    int currentMilestone = 0;

    //Camera for the scene
    public Camera mainCamera = new Camera(0,0,14);

    //Size of the render area
    float[] windowSize = new float[2];

    //Called on initialization of the canvas, initialises OpenGL features
    @Override
    public void init(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        gl.glEnable(GL3.GL_DEPTH_TEST);
        gl.glClearColor(0.85f,0.85f,0.85f,1.0f);

        //Setup scene light source
        gl.glEnable(gl.GL_LIGHT0);
        gl.glEnable(gl.GL_LIGHTING);
        gl.glEnable(gl.GL_NORMALIZE);

        float[] lightPos = {0,0,0,1};
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_POSITION, lightPos, 0);

        float[] ambient = {0.2f,0.2f,0.2f,0f};
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_AMBIENT, ambient, 0);

        float[] diffuse = {0.65f,0.65f,0.65f,0f};
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_DIFFUSE, diffuse, 0);

        float[] specular = {0.4f,0.4f,0.4f,0f};
        gl.glLightfv(gl.GL_LIGHT0, gl.GL_SPECULAR, specular, 0);

        //Setup camera
        mainCamera.setSpeed(2.5f);
        mainCamera.updateFrameRate(30);
        resetCamera();

    }

    //Called on destruction of the canvas
    @Override
    public void dispose(GLAutoDrawable drawable) {

    }


    //Draw the given milestone by taking a subsection of sphereList to draw in the next frame
    public void setMilestone(int milestoneIndex){
        drawList.clear();
        currentMilestone = milestoneIndex;
        if (sphereList.size() == 0) {return;}
        if (milestoneIndex < milestoneClusters.size()){
            for (Cluster cluster : milestoneClusters.get(milestoneIndex)){
                drawList.addAll(cluster.getSpheres());
            }
        }
    }


    //Generate a collection of clusters for each milestone used to keep
    //spheres which are highly dependant on each-other in close proximity
    private ArrayList<Cluster> generateClusters(ArrayList<ClassSphere> spheres){
        ArrayList<Cluster> clusters = new ArrayList<Cluster>();
        ArrayList<ClassSphere> newSpheres = new ArrayList<ClassSphere>();
        newSpheres.addAll(spheres);

        //Calculate an ideal nunber of clusters to generate for the number of sphere sin the scene
        int numSpheresPerCluster = 10;
        int numClusters = 1+(spheres.size() / numSpheresPerCluster);

        //Create the cluster instances with a temporary position
        for (int i=0; i < numClusters; ++i){
            Cluster newCluster = new Cluster(0,0,0, i);
            clusters.add(newCluster);
        }


        //Identify spheres that were added in this milestone
        for (ClassSphere newSphere : spheres){
            boolean isNewSphere = true;
            for (ArrayList<ClassSphere> milestoneSpheres : sphereList){
                for (ClassSphere sphere : milestoneSpheres) {
                    if (sphere.getName().equals(newSphere.getName())){
                        isNewSphere = false;
                        clusters.get(sphere.getCluster()).addSphere(newSphere);
                        newSpheres.remove(newSphere);
                    }
                }
                if (!isNewSphere){break;}
            }
        }


        //Find an ideal cluster for each new sphere and add it to that cluster
        //This will generate a position for the new sphere within the cluster
        while (newSpheres.size() != 0){
            for (Cluster cluster : clusters){

                int highestLinks = 0;
                int bestSphere = 0;
                for (int i=0; i < newSpheres.size(); ++i){
                    if (cluster.getSize() == 0){break;}
                    int numLinks = cluster.getLinks(newSpheres.get(i));
                    if (numLinks >= highestLinks){
                        highestLinks = numLinks;
                        bestSphere = i;
                    }
                }
                cluster.addSphere(newSpheres.get(bestSphere));
                newSpheres.remove(newSpheres.get(bestSphere));
                if (newSpheres.size() == 0){break;}
            }
        }




        //Calculate the largest cluster size which is used as a minimum space between clusters
        float largestClusterRadius = 0;
        for (Cluster cluster : clusters){
            if (cluster.getClusterRadius() > largestClusterRadius){
                largestClusterRadius = cluster.getClusterRadius();
            }
        }

        float[] randomPosition = {0,0,0};

        float minDistance = largestClusterRadius * 2f;
        float maxDistance = (minDistance*2) * (numClusters*0.5f);

        if (clusters.size() == 1){return clusters;}

        //Find a valid position for each cluster avoiding intersections
        for (Cluster cluster : clusters){

            int maxChecks = 100;
            boolean foundPosition = false;

            //Find a location which is far enough away from the center of the scene
            while (!foundPosition) {
                if (maxChecks <= 0) {
                    throw new RuntimeException("Could not find a valid position for a cluster!");
                }
                --maxChecks;

                //Find a random location within minimum and maximum bounds
                float distFromCenter = 0;

                Random randomGenerator = new Random();
                while (distFromCenter < minDistance) {

                    randomPosition[0] = (randomGenerator.nextFloat() * maxDistance) - maxDistance / 2;
                    randomPosition[1] = (randomGenerator.nextFloat() * maxDistance) - maxDistance / 2;
                    randomPosition[2] = (randomGenerator.nextFloat() * maxDistance) - maxDistance / 2;
                    distFromCenter = getMagnitude(randomPosition);
                }


                boolean isValidPosition = true;
                //Make sure the cluster does not intersect other clusters
                for (Cluster otherCluster : clusters){
                    if (cluster.equals(otherCluster)){ continue; }
                    if (getMagnitude(otherCluster.getPosition()) < minDistance){continue;}
                    float[] clusterVec = {
                            otherCluster.getPosX() - cluster.getPosX(),
                            otherCluster.getPosY() - cluster.getPosY(),
                            otherCluster.getPosZ() - cluster.getPosZ()};
                    float clusterDistance = getMagnitude(clusterVec);
                    float radiusDistance = otherCluster.getClusterRadius() + cluster.getClusterRadius();

                    if (clusterDistance <= radiusDistance){
                        isValidPosition = false;
                        break;
                    }
                }

                if (isValidPosition){
                    foundPosition = true;
                    cluster.setPosition(randomPosition[0], randomPosition[1], randomPosition[2]);
                }
            }
        }

        return clusters;
    }


    //Re-builds the sphereList with new spheres and milestones
    public void createSpheres(ArrayList<Milestone> milestones){
        sphereList.clear();
        milestoneClusters.clear();
        setMilestone(0);
        if (milestones.size() == 0){return;}

        //Create spheres for each class in each milestone
        for (int i=0; i < milestones.size(); ++i){
            ArrayList<ClassInstance> newClasses = milestones.get(i).getClasses();
            ArrayList<ClassSphere> newSpheres = new ArrayList<ClassSphere>();

            for (ClassInstance newClass : newClasses){
                newSpheres.add(new ClassSphere(0, 0, 0, newClass));
            }

            //Provide each sphere with references to dependant spheres
            for (ClassSphere sphere : newSpheres){
                ArrayList<ClassInstance> dependencies = sphere.getRepresentedClass().getOutgoingDependencies();

                for (ClassSphere otherSphere : newSpheres){
                    if (dependencies.contains(otherSphere.getRepresentedClass())) {
                        if (!sphere.getDependencySpheres().contains(otherSphere)) {
                            sphere.addDependencySphere(otherSphere);
                        }
                    }
                }
            }

            ArrayList<Cluster> newClusters = generateClusters(newSpheres);


            for (Cluster cluster : newClusters){
                sphereList.add(new ArrayList<ClassSphere>());
                cluster.placeSpheres(sphereList, i);
            }

            milestoneClusters.add(newClusters);
        }

        //Calculate the center point of the scene
        calculateCenter();

        mainCamera.resetPosition();

        //Re-apply all transformations for selected metrics
        setSizeMetric(sizeMetric);
        setColourMetric(colourMetric);
        setLinkMetric(linkMetric);
        showGuides(drawGuides);
        setMilestone(0);
    }


    //Calculates the center of mass of the entire scene and saves it to sceneCenter
    private void calculateCenter() {
        float[] sceneCenter = new float[3];
        int totalSpheres = 0;
        for (ArrayList<ClassSphere> milestoneSpheres : sphereList) {
            for (ClassSphere sphere : milestoneSpheres) {
                sceneCenter[0] += sphere.getPosX();
                sceneCenter[1] += sphere.getPosY();
                sceneCenter[2] += sphere.getPosZ();
                totalSpheres++;
            }
        }

        if (totalSpheres > 0) {
            sceneCenter[0] /= totalSpheres;
            sceneCenter[1] /= totalSpheres;
            sceneCenter[2] /= totalSpheres;
        } else {
            sceneCenter[0] = 0;
            sceneCenter[1] = 0;
            sceneCenter[2] = 0;
        }

        mainCamera.setSceneCenter(sceneCenter);
    }


    //Get the length of a vector
    public static float getMagnitude(float[] vec){
        return abs((float)sqrt(pow(vec[0],2) + pow(vec[1],2) + pow(vec[2],2)));
    }


    //Updates all spheres to change colour to reflect the given metric
    public void setColourMetric(ClassMetric metric){
        for (ArrayList<ClassSphere> milestoneSpheres : sphereList){
            for (ClassSphere sphere : milestoneSpheres){
                sphere.setColourFromMetric(metric);
                sphere.printColourMetric();
            }
        }
        System.out.println("");
        colourMetric = metric;
    }


    //Updates all spheres to scale to reflect the given metric
    public void setSizeMetric(ClassMetric metric){
        for (ArrayList<ClassSphere> milestoneSpheres : sphereList){
            for (ClassSphere sphere : milestoneSpheres){
                sphere.setSizeFromMetric(metric);
                sphere.printSizeMetric();
            }
        }
        System.out.println("");
        sizeMetric = metric;
    }


    //Updates all links to change colour to match the given metric
    public void setLinkMetric(DependencyMetric metric){
        for (ArrayList<ClassSphere> milestoneSpheres : sphereList){
            for (ClassSphere sphere : milestoneSpheres){
                sphere.setLinkColourFromMetric(metric);
                sphere.printDependencyMetric();
            }
        }
        System.out.println("");
        linkMetric = metric;
    }



    //Tells all spheres in the scene to draw their respective metric guides
    public void showGuides(boolean state){
        for (ArrayList<ClassSphere> milestoneSpheres : sphereList){
            for (ClassSphere sphere : milestoneSpheres){
                sphere.showGuides(state);
            }
        }
        drawGuides = state;
    }


    //Reset the camera to default position and perspective
    public void resetCamera(){
        float numSpheres = sphereList.get(currentMilestone).size();
        calculateCenter();
        mainCamera.resetPosition();

        mainCamera.setPosition(mainCamera.getPosX(),mainCamera.getPosY(), (numSpheres*4f) - (1f * numSpheres));
        if (numSpheres == 1){mainCamera.setPosZ(6);}
        mainCamera.setSpeed(0.18f * (3.5f * numSpheres));
    }


    //Called on a repaint or draw event on the canvas
    //Draws each sphere in the scene
    @Override
    public void display(GLAutoDrawable drawable) {
        GL2 gl = drawable.getGL().getGL2();
        GLU glu = new GLU();


        //Clear colour and depth buffers each frame
        gl.glClear(gl.GL_COLOR_BUFFER_BIT | gl.GL_DEPTH_BUFFER_BIT);

        //Set the perspective projection
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        float aspectRatio = windowSize[0] / windowSize[1];
        glu.gluPerspective(45.0f, aspectRatio, 0.01f,1000);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glPushMatrix();
        //Transform scene to reflect camera movements
        mainCamera.doMovement();


        //Draw each sphere in the currently selected milestone
        for(ClassSphere sphere : drawList){
            //Apply camera translation and rotations
            mainCamera.applyTransformations(gl,glu);
            //Draw the sphere and its components
            sphere.draw(drawable, 20, mainCamera);
            gl.glPopMatrix();
            gl.glPushMatrix();
        }
        gl.glPopMatrix();
    }

    //Called when the canvas is resized, scales viewport
    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

        GL2 gl = drawable.getGL().getGL2();
        GLU glu = new GLU();
        gl.glViewport(x,y,width,height);
        windowSize[0] = width;
        windowSize[1] = height;
    }
}
