package com.metricvis.Rendering;

//Base class for all drawable 3D models
//Used to control position storage and access
public abstract class Model {
    private float[] position = new float[3];

    //Create all drawable models with a position in 3D space
    Model(float x, float y, float z){
        setPosition(x,y,z);
    }


    //Set the position of them model
    public void setPosition(float x, float y, float z){
        position[0] = x;
        position[1] = y;
        position[2] = z;
    }


    //Get individual axis positions
    public float getPosX(){ return position[0];}
    public float getPosY(){ return position[1];}
    public float getPosZ(){ return position[2];}

    //Set individual axis positions
    public void setPosX(float newX){position[0] = newX;}
    public void setPosY(float newY){position[1] = newY;}
    public void setPosZ(float newZ){position[2] = newZ;}

    //Get full position
    public float[] getPosition(){ return position;}
}
