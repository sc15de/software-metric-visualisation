package com.metricvis.Rendering;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.math.VectorUtil;

import java.util.ArrayList;

//Camera class used to transform the visualisation scene to represent the user's chosen perspective
public class Camera extends Model {

    //Available movement directions for the camera
    public enum MOVE_DIRECTION{
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,
        UP,
        DOWN,
    }

    //Camera sight vector
    private float[] cameraLook = {0,0,-1};
    //Camera up vector
    private float[] upVec = {0,1,0};

    //Center of the rendered scene
    private float[] sceneCenter = {0,0,0};

    //Un-modulated camera speed (Distance per second)
    private float cameraSpeed = 2.5f;

    //Camera movement speed after scaling with frame rate
    private float scaledSpeed = 1;

    //Last known scene FPS
    private float FPS = 30;

    //Scene rotations
    private float sceneRotationYaw = 0;
    private float rotationDirectionYaw = 0;
    private float sceneRotationPitch = 0;
    private float rotationDirectionPitch = 0;

    //List of active movements
    //Camera will move in all contained directions when updated
    private ArrayList<MOVE_DIRECTION> moveDirections = new ArrayList<MOVE_DIRECTION>();

    //Create camera with eye position
    Camera(float x, float y, float z) {
        super(x, y, z);
    }


    //Add a movement to be applied on the next camera update
    public void addMovement(MOVE_DIRECTION dir){
        if (moveDirections.contains(dir) == false){
            moveDirections.add(dir);
        }
    }

    //Remove a movement from the camera to be applied next update
    public void removeMovement(MOVE_DIRECTION dir){ moveDirections.remove(dir);}

    //Set the rotation direction when rotating the scene, Left: -1, Right: 1, Stopped: 0
    public void setSceneRotationYaw(int rotation){ rotationDirectionYaw = rotation; }
    public void setSceneRotationPitch(int rotation){ rotationDirectionPitch = rotation; }


    //Perform queued movement commands translating the camera position and rotating
    public void doMovement(){
        for (MOVE_DIRECTION movement : moveDirections){
            switch(movement){
                case FORWARD: moveForward(); break;
                case BACKWARD: moveBackward(); break;
                case LEFT: moveLeft(); break;
                case RIGHT: moveRight(); break;
                case UP: moveUp(); break;
                case DOWN: moveDown(); break;
            }
        }
        sceneRotationYaw += 2*rotationDirectionYaw;
        sceneRotationPitch += 2*rotationDirectionPitch;
    }


    //Modifies the current scene render to apply all performed movements and rotations
    public void applyTransformations(GL2 gl, GLU gnu){
        gl.glMatrixMode(GL2.GL_PROJECTION_MATRIX);

        //Apply movements
        gnu.gluLookAt(
                getPosX(), getPosY(), getPosZ(),
                getPosX()+getLookX(), getPosY()+getLookY(), getPosZ()+getLookZ(),
                upVec[0],upVec[1],upVec[2]);

        //Apply translations
        doRotations(gl);
    }


    //Perform camera scene rotation
    public void doRotations(GL2 gl){
        gl.glTranslatef(sceneCenter[0],sceneCenter[1],sceneCenter[2]);
        gl.glRotatef(sceneRotationYaw,0,1,0);
        gl.glRotatef(sceneRotationPitch,1,0,0);
        gl.glTranslatef(-sceneCenter[0],-sceneCenter[1],-sceneCenter[2]);
    }


    //Undo rotations applied to the scene
    public void undoRotations(GL2 gl){
        gl.glTranslatef(sceneCenter[0],sceneCenter[1],sceneCenter[2]);
        gl.glRotatef(-sceneRotationYaw,0,1,0);
        gl.glRotatef(-sceneRotationPitch,1,0,0);
        gl.glTranslatef(-sceneCenter[0],-sceneCenter[1],-sceneCenter[2]);
    }



    //Resets the camera view to face the scene
    public void resetPosition(){
        sceneRotationYaw = 0;
        sceneRotationPitch = 0;
        setPosition(sceneCenter[0],sceneCenter[1],sceneCenter[2] + 14);
    }


    //Update the recorded frame rate to scale the movement speed
    public void updateFrameRate(float frameRate){
        FPS = frameRate;
        scaledSpeed = cameraSpeed / frameRate;
    }


    //Set the distance per second of the camera movements
    public void setSpeed(float speed){
        if (cameraSpeed + speed < 0){
            cameraSpeed = 0;
        }else{
            cameraSpeed = speed;
        }
        updateFrameRate(FPS);
    }


    //Get the camera speed
    public float getSpeed(){return cameraSpeed;}

    //Get individual axis values for camera look vector
    public float getLookX(){ return cameraLook[0]; }
    public float getLookY(){ return cameraLook[1]; }
    public float getLookZ(){ return cameraLook[2]; }
    public float[] getLook(){ return cameraLook; }

    //Translate camera forwards
    private void moveForward(){
        setPosX(getPosX() + (cameraLook[0] * scaledSpeed));
        setPosY(getPosY() + (cameraLook[1] * scaledSpeed));
        setPosZ(getPosZ() + (cameraLook[2] * scaledSpeed));
    }


    //Translate camera backwards
    private void moveBackward(){
        setPosX(getPosX() - (cameraLook[0] * scaledSpeed));
        setPosY(getPosY() - (cameraLook[1] * scaledSpeed));
        setPosZ(getPosZ() - (cameraLook[2] * scaledSpeed));
    }


    //Translate camera left
    private void moveLeft() {
        float[] newPos = new float[3];
        VectorUtil.crossVec3(newPos,cameraLook, upVec);
        VectorUtil.normalizeVec3(newPos);
        setPosX(getPosX() - (newPos[0] * scaledSpeed));
        setPosY(getPosY() - (newPos[1] * scaledSpeed));
        setPosZ(getPosZ() - (newPos[2] * scaledSpeed));
    }


    //Translate camera right
    private void moveRight() {
        float[] newPos = new float[3];
        VectorUtil.crossVec3(newPos,cameraLook, upVec);
        VectorUtil.normalizeVec3(newPos);
        setPosX(getPosX() + (newPos[0] * scaledSpeed));
        setPosY(getPosY() + (newPos[1] * scaledSpeed));
        setPosZ(getPosZ() + (newPos[2] * scaledSpeed));
    }


    //Translate camera up
    private void moveUp() {
        float[] newPos = new float[3];
        VectorUtil.crossVec3(newPos,cameraLook, upVec);
        float[] tiltedUpVec = new float[3];
        VectorUtil.crossVec3(tiltedUpVec,newPos, cameraLook);
        VectorUtil.normalizeVec3(tiltedUpVec);
        setPosX(getPosX() + (tiltedUpVec[0] * scaledSpeed));
        setPosY(getPosY() + (tiltedUpVec[1] * scaledSpeed));
        setPosZ(getPosZ() + (tiltedUpVec[2] * scaledSpeed));
    }


    //Translate camera down
    private void moveDown() {
        float[] newPos = new float[3];
        VectorUtil.crossVec3(newPos,cameraLook, upVec);
        float[] tiltedUpVec = new float[3];
        VectorUtil.crossVec3(tiltedUpVec,newPos, cameraLook);
        VectorUtil.normalizeVec3(tiltedUpVec);
        setPosX(getPosX() - (tiltedUpVec[0] * scaledSpeed));
        setPosY(getPosY() - (tiltedUpVec[1] * scaledSpeed));
        setPosZ(getPosZ() - (tiltedUpVec[2] * scaledSpeed));
    }


    //Set the center of the rendered scene
    public void setSceneCenter(float[] center){ sceneCenter = center;}

    //Get the center of the scene
    public float[] getSceneCenter(){ return sceneCenter;}


}
