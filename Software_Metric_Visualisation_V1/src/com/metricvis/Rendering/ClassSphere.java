package com.metricvis.Rendering;

import com.jogamp.opengl.GL;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.glu.GLUquadric;
import com.jogamp.opengl.math.VectorUtil;
import com.jogamp.opengl.util.awt.TextRenderer;
import com.metricvis.instances.ClassInstance;
import com.metricvis.metrics.ClassMetric;
import com.metricvis.metrics.DependencyMetric;
import com.metricvis.metrics.SoftwareMetric;

import java.awt.*;
import java.nio.FloatBuffer;
import java.util.ArrayList;


//Drawable sphere which represents a class and can be transformed in size and colour to represent chosen metrics
//Also draws text containing the represented class names and draws arrows to spheres representing dependant classes
//Also draws guide circles representing the ideal size of the sphere in accordance to the selected metric
public class ClassSphere extends Model{
    //The class instance represented by this sphere
    private ClassInstance representedClass;

    //List of spheres representing out-going dependencies
    private ArrayList<ClassSphere> outDependencies = new ArrayList<ClassSphere>();

    //List of colours for each dependency link
    ArrayList<ArrayList<Float>> dependencyColours = new ArrayList<ArrayList<Float>>();

    //Text render object used to render text in 3D space
    private static TextRenderer textRenderer = new TextRenderer(new Font("Arial", Font.BOLD, 36));

    //Current colour of the sphere
    private float[] colour = {0,1,0};

    //Current scale of the sphere
    private float scale = 1.0f;

    //Radius of the sphere before scaling
    private float radius = 0.5f;

    //Draws guide circles when set to true
    private boolean doGuideDraw = false;

    //ID of the cluster this sphere belongs to
    int clusterID = -1;

    //Computed metric values for each dimension
    private SoftwareMetric sizeMetric = null;
    private SoftwareMetric colourMetric = null;
    private ArrayList<SoftwareMetric> dependencyMetrics = null;
    private DependencyMetric dependencyMetricType = null;

    //Create the sphere with a position and class instance to represent
    ClassSphere(float x, float y, float z, ClassInstance sourceClass){
        super(x,y,z);
        representedClass = sourceClass;
    }

    //Add a sphere representing an outward dependency
    public void addDependencySphere(ClassSphere sphere){
        outDependencies.add(sphere);
    }

    //Get the name of the sphere and represented class
    public String getName() {
        return representedClass.getName();
    }
    

    //Assigns a colour value to the given metric
    //The select colour is chosen by comparing the metric value for the represented class with the target
    //Blue is too low, red is too high, green is ideal
    private float[] getColourFromMetric(SoftwareMetric metric){
        if (metric.getTarget() < 0){
            return getColourFromSpectrum(0.0f);
        }
        float differencePercent = (metric.getValue() / metric.getTarget());
        return getColourFromSpectrum((1.0f * differencePercent) - 1.0f);
    }


    //Set the metric which will be represented by the sphere's colour
    public void setColourFromMetric(ClassMetric metricType){
        colourMetric = metricType.calculateMetric(representedClass);
        colour = getColourFromMetric(colourMetric);
    }


    //Set sphere colour bases on spectrum value
    //Continuous colour
    //Red represents high spectrum, blue represents low spectrum, green represents middle
    //Max at spectrumValue = -1 or spectrumValue = 1
    private float[] getColourFromSpectrum(float spectrumValue){
        float[] newColour = {0,1,0};
        if (spectrumValue > 1.0f){spectrumValue = 1.0f;}
        if (spectrumValue < -1.0f){spectrumValue = -1.0f;}

        float maxColourStrength = 0.5f;

        spectrumValue *= 0.75f;

        if (spectrumValue <= 0.0f){
            newColour[1] = spectrumValue + 1.0f;
            newColour[0] = 0.0f;
            newColour[2] = 1.0f - newColour[1];
        }else{
            newColour[1] = 1 - spectrumValue;
            newColour[0] = 1.0f - newColour[1];
            newColour[2] = 0.0f;
        }
        return newColour;
    }


    //Set the metric which will be represented by the sphere's size
    public void setSizeFromMetric(ClassMetric metricType){
        sizeMetric = metricType.calculateMetric(representedClass);
        if (sizeMetric.getTarget() < 0){
            scale = 1.0f;
            return;
        }
        float differencePercent = (sizeMetric.getValue() / sizeMetric.getTarget());
        scale = 1.0f * differencePercent;

        float maxScaleChange = getMaxScale() - 1.0f;
        if (scale < 0.0f + maxScaleChange){scale = 1.0f - maxScaleChange;}
        if( scale > 1.0f + maxScaleChange){scale = 1.0f + maxScaleChange;}
    }


    //Set the metric which will be represented by the link between this sphere and dependant sphere's
    public void setLinkColourFromMetric(DependencyMetric metricType){
        dependencyColours.clear();
        dependencyMetricType = metricType;
        dependencyMetrics = metricType.calculateMetrics(representedClass);
        if (dependencyMetrics.size() == 0){return;}

        ArrayList<ClassSphere> newSphereDependencies =  new ArrayList<ClassSphere>();


        for (int i=dependencyMetrics.size()-1; i >=0; --i){
            ArrayList<Float> newColour = new ArrayList<Float>();
            float[] newColourList = getColourFromMetric(dependencyMetrics.get(dependencyMetrics.size()-i-1));
            newColour.add(newColourList[0]);
            newColour.add(newColourList[1]);
            newColour.add(newColourList[2]);
            for (ClassSphere otherSphere : outDependencies){
                String otherSphereName = otherSphere.getRepresentedClass().getName();
                String otherClassName = representedClass.getOutgoingDependencies().get(dependencyMetrics.size()-i-1).getName();
                if (otherSphereName.equals(otherClassName)){
                    newSphereDependencies.add(otherSphere);
                    break;
                }
            }

            dependencyColours.add(newColour);
        }

        outDependencies = newSphereDependencies;

    }


    //Print computed metric value for the size dimension
    public void printSizeMetric(){
        System.out.println("Size Metric: Class: " + getName() + ", " + sizeMetric.getName() + ": Target: " +
                sizeMetric.getTarget() + ", Value: " + sizeMetric.getValue());
    }


    //Print computed metric value for the colour dimension
    public void printColourMetric(){
        System.out.println("Colour Metric: Class: " + getName() + ", " + colourMetric.getName() + ": Target: " +
                colourMetric.getTarget() + ", Value: " + colourMetric.getValue());
    }


    //Print computed metric value for each dependency
    public void printDependencyMetric(){
        String printTitle = "Dependency Metric: Class: " + getName() + ", " + dependencyMetricType.getName() +
                ": Target: " + dependencyMetricType.getTarget() + ":";

        if (dependencyMetrics.size() == 0){printTitle += " (No Dependencies)";}

        System.out.println(printTitle);

        for (int i=0; i < dependencyMetrics.size(); ++i){
            System.out.println("  " + getDependencySpheres().get(i).getName() + ": " + dependencyMetrics.get(i).getValue());
        }
    }



    //Draw the sphere and its components in the given render context
    public void draw(GLAutoDrawable drawable, int resolution, Camera camera){
        GL2 gl = drawable.getGL().getGL2();
        GLU glu = new GLU();

        //Set the sphere colour and lighting material properties
        float[] materialColour = {colour[0], colour[1], colour[2], 1.0f};
        float[] specularReflectivity = {0.0f,0.0f,1.0f,1.0f};

        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_AMBIENT_AND_DIFFUSE, materialColour,0);
        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_SPECULAR, specularReflectivity,0);
        gl.glMaterialf(gl.GL_FRONT_AND_BACK, gl.GL_SHININESS, 16);

        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glPushMatrix();
        //Move the sphere to its intended position
        gl.glTranslatef(getPosX(), getPosY(), getPosZ());
        //Scale uniformly to represent the chosen size metric
        gl.glScalef(scale, scale, scale);

        //Use GLU to create and draw a sphere with the given resolution
        GLUquadric sphere = glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(sphere, GLU.GLU_FILL);
        glu.gluQuadricNormals(sphere, GLU.GLU_FLAT);
        glu.gluQuadricOrientation(sphere, GLU.GLU_OUTSIDE);
        glu.gluSphere(sphere, radius, resolution, resolution);
        glu.gluDeleteQuadric(sphere);


        gl.glPopMatrix();;
        //Draw dependency arrows to dependant spheres
        drawLinks(gl, camera);
        gl.glTranslatef(getPosX(), getPosY(), getPosZ());
        //Draw guide circles if enabled
        if (doGuideDraw){drawGuide(gl, camera);}
        //Draw text containing the name of the represented class
        drawText(gl, camera);
    }


    //Draw the text containing the name of the represented class
    private void drawText(GL2 gl, Camera camera){
        float textDistanceFromCenter = (radius *scale) + 0.2f;

        //Draw the text
        textRenderer.begin3DRendering();
        textRenderer.setColor(Color.BLACK);
        textRenderer.setSmoothing(true);
        String textContent = getName();
        float textWidth = (float)textRenderer.getBounds(textContent).getWidth();

        float[] originalCenter = camera.getSceneCenter();
        camera.setSceneCenter(new float[]{0,0,0});
        camera.undoRotations(gl);
        textRenderer.draw3D(textContent, -((textWidth / 200)/2), 0, 0+textDistanceFromCenter, 0.005f);
        textRenderer.end3DRendering();
        camera.doRotations(gl);
        camera.setSceneCenter(originalCenter);
    }



    //Draws arrows to spheres representing dependant classes
    private void drawLinks(GL2 gl, Camera camera){
        float[] cameraPos = camera.getPosition();

        for (int i=0; i < outDependencies.size(); ++i){
            ClassSphere sphere = outDependencies.get(i);
            //Apply colour for this dependency link calculated using chosen metric
            ArrayList<Float> dependencyColour = dependencyColours.get(i);
            float[] dependencyColourList = {dependencyColour.get(0),dependencyColour.get(1),dependencyColour.get(2),1.0f};

            gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_AMBIENT_AND_DIFFUSE, dependencyColourList,0);

            //Get the vector to the camera
            float[] eyeVec = {cameraPos[0] - getPosX(), cameraPos[1] - getPosY(), cameraPos[2] - getPosZ()};
            //Get the vector to the sphere
            float[] otherVec = {sphere.getPosX() - getPosX(), sphere.getPosY() - getPosY(), sphere.getPosZ() - getPosZ()};

            //Create a plane to draw the base of the arrow
            float[] planeX = new float[3];
            float[] planeY = new float[3];
            VectorUtil.crossVec3(planeX, eyeVec, otherVec);
            VectorUtil.crossVec3(planeY, planeX, otherVec);

            VectorUtil.normalizeVec3(planeX,0);
            VectorUtil.normalizeVec3(planeY,0);

            float baseRadius = ((radius * 0.5f) * scale);
            int angleInc = 15;
            int currentAngle = 0;

            float[] lastPoint = {getPosX()+(planeX[0]*baseRadius), getPosY()+(planeX[1]*baseRadius), getPosZ()+(planeX[2]*baseRadius)};
            float[] origin = {getPosX(), getPosY(), getPosZ()};

            //Draw base circle and join each point on the circumference to the target sphere
            gl.glBegin(gl.GL_TRIANGLES);
            while (currentAngle < 360 + angleInc) {
                currentAngle += angleInc;
                float circleX = baseRadius * (float) Math.cos(currentAngle * 3.142 / 180);
                float circleY = baseRadius * (float) Math.sin(currentAngle * 3.142 / 180);

                float[] thisPoint = {
                        getPosX() + (planeX[0]*circleX) + (planeY[0] * circleY),
                        getPosY() + (planeX[1]*circleX) + (planeY[1] * circleY),
                        getPosZ() + (planeX[2]*circleX) + (planeY[2] * circleY)};


                gl.glNormal3f(eyeVec[0], eyeVec[1], eyeVec[2]);
                //Draw circle base
                gl.glVertex3f(origin[0],origin[1],origin[2]);
                gl.glVertex3f(thisPoint[0], thisPoint[1], thisPoint[2]);
                gl.glVertex3f(lastPoint[0], lastPoint[1], lastPoint[2]);

                float[] normal = new float[3];
                float[] triVec1 = {thisPoint[0] - sphere.getPosX(), thisPoint[1] - sphere.getPosY(), thisPoint[2] - sphere.getPosZ()};
                float[] triVec2 = {lastPoint[0] - sphere.getPosX(), lastPoint[1] - sphere.getPosY(), lastPoint[2] - sphere.getPosZ()};
                VectorUtil.crossVec3(normal, triVec1, triVec2);

                //Draw arrow to target sphere
                gl.glNormal3f(normal[0], normal[1], normal[2]);
                gl.glVertex3f(sphere.getPosX(), sphere.getPosY(), sphere.getPosZ());
                gl.glVertex3f(thisPoint[0], thisPoint[1], thisPoint[2]);
                gl.glVertex3f(lastPoint[0], lastPoint[1], lastPoint[2]);

                lastPoint = thisPoint;
            }
            gl.glEnd();
        }
    }


    //Draws a hollow circle around the sphere to represent the ideal sphere size for the currently selected size metric
    private void drawGuide(GL2 gl, Camera camera){

        gl.glMaterialfv(gl.GL_FRONT_AND_BACK, gl.GL_AMBIENT_AND_DIFFUSE, new float[]{0.8f,0.8f,0.8f},0);

        float guideOuterRadius = radius + 0.1f;
        float guideInnerRadius = radius;

        int angleInc = 15;
        int currentAngle = 0;

        float[] lastOuterPoint = {guideOuterRadius,0,0};
        float[] lastInnerPoint = {guideOuterRadius,0,0};

        //Draw two circles and join the circumferences to form a hollow circle
        gl.glDisable(GL.GL_DEPTH_TEST);
        float[] originalCenter = camera.getSceneCenter();
        camera.setSceneCenter(new float[]{0,0,0});
        camera.undoRotations(gl);
        gl.glBegin(gl.GL_QUADS);
        gl.glNormal3f(0,0,0);
        while (currentAngle < 360 + angleInc) {
            currentAngle += angleInc;
            float outerCircleX = guideOuterRadius * (float) Math.cos(currentAngle * 3.142 / 180);
            float outerCircleY = guideOuterRadius * (float) Math.sin(currentAngle * 3.142 / 180);
            float innerCircleX = guideInnerRadius * (float) Math.cos(currentAngle * 3.142 / 180);
            float innerCircleY = guideInnerRadius * (float) Math.sin(currentAngle * 3.142 / 180);

            float[] outerPoint = {outerCircleX, outerCircleY, 0};
            float[] innerPoint = {innerCircleX, innerCircleY, 0};

            gl.glVertex3f(outerPoint[0], outerPoint[1], outerPoint[2]);
            gl.glVertex3f(innerPoint[0], innerPoint[1], innerPoint[2]);
            gl.glVertex3f(lastInnerPoint[0], lastInnerPoint[1], lastInnerPoint[2]);
            gl.glVertex3f(lastOuterPoint[0], lastOuterPoint[1], lastOuterPoint[2]);

            lastOuterPoint = outerPoint;
            lastInnerPoint = innerPoint;

        }
        gl.glEnd();
        camera.doRotations(gl);
        camera.setSceneCenter(originalCenter);
        gl.glEnable(GL.GL_DEPTH_TEST);
    }


    //Get a list of all known spheres represent dependant classes
    public ArrayList<ClassSphere> getDependencySpheres(){
        return outDependencies;
    }

    //Get the maximum scale modifier
    public float getMaxScale(){
        return 1.75f;
    }

    //Enable or disable drawing of guide circles
    public void showGuides(boolean state){
        doGuideDraw = state;
    }

    //Get the class represented by this sphere
    public ClassInstance getRepresentedClass(){
        return representedClass;
    }

    //Set the ID of the cluster this sphere belongs to
    public void setCluster(int ID){
        clusterID = ID;
    }

    //Get the ID of the cluster this sphere belongs to
    public int getCluster(){return clusterID;}
}
