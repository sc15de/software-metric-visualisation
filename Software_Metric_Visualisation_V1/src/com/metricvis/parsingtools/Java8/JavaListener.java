package com.metricvis.parsingtools.Java8;

import com.metricvis.instances.ClassInstance;
import com.metricvis.instances.MethodInstance;
import com.metricvis.instances.PackageInstance;
import com.metricvis.parsingtools.Java8.generated.Java8Parser;
import com.metricvis.parsingtools.Java8.generated.Java8ParserBaseListener;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

//Listener class for a JAVA parser
//Uses a collection of events to respond to rules identified in the parse tree created by the JAVA parser object
//Uses the result parse tree and rules to build class and method representations for metric computation.
public class JavaListener extends Java8ParserBaseListener {

    //Name of the file source java file being parsed
    String sourceFileName;

    //Parser object for recognising java code rules
    Java8Parser parser;

    //List of classes found by the parser
    private ArrayList<ClassInstance> classStructure = new ArrayList<ClassInstance>();

    //List of found packages
    private PackageInstance sourcePackage = null;

    //Build listener to respond to rules for the given parser
    public JavaListener(Java8Parser usedParser, String fileName){
        sourceFileName = fileName;
        parser = usedParser;
    }

    //Get the package the java file belongs to
    public PackageInstance getSourcePackage(){
        return sourcePackage;
    }

    //Get the generated list of classes
    public ArrayList<ClassInstance> getClasses(){
        return classStructure;
    }

    //Returns the class instance associated with the parent class declaration rule
    private ClassInstance findParentClass(ParserRuleContext ctx){

        //Find the node of the parent class declaration
        ParserRuleContext classParent = findFirstParentRule(ctx,"normalClassDeclaration", "normalInterfaceDeclaration");


        String className = getClassName(classParent);
        //Find the class instance with the matching class name
        for (int i= classStructure.size()-1; i >= 0; --i){
            if (classStructure.get(i).getName().equals(className)){
                return classStructure.get(i);
            }
        }

        return null;
    }


    //Finds the parent method instance at the given node
    private MethodInstance findParentMethod(ParserRuleContext ctx){
        try {
            //Find the parent class or interface containing this method
            ParserRuleContext methodParent = findFirstParentRule(ctx, "methodDeclaration",
                    "interfaceMethodDeclaration", "constructorDeclaration", "classBodyDeclaration");

            String foundName = parser.getRuleNames()[methodParent.getRuleIndex()];
            if (foundName.equals("classBodyDeclaration")){
                System.out.println("No parent method found");
                return new MethodInstance("-UNKNOWN METHOD-");
            }

            String methodName = getMethodName(methodParent);

            //Find a method instance with a matching name by searching each known class
            MethodInstance parentMethod = null;
            for (int i = classStructure.size() - 1; i >= 0; --i) {
                parentMethod = classStructure.get(i).getMethod(methodName);
                if (parentMethod != null) {
                    break;
                }
            }
            return parentMethod;
        }catch(NullPointerException e){
            e.printStackTrace();
            return new MethodInstance("-UNKNOWN METHOD-");
        }

    }


    //Perform a depth first search to create a list of leaf value strings
    private ArrayList<String> leafSearch(ParseTree tree, int maxDepth){
        ArrayList<String> leaves = new ArrayList<String>();
        if (maxDepth == 0){return leaves;}

        if (tree.getChildCount() != 0){
            for (int i=0; i < tree.getChildCount();++i){
                leaves.addAll(leafSearch(tree.getChild(i), maxDepth-1));
            }
            return leaves;
        }

        leaves.add(tree.getText());
        return leaves;
    }


    //Get the name of a class from a class declaration node
    private String getClassName(ParserRuleContext classDeclarationNode){
        try {
            for (int i = 0; i < classDeclarationNode.getChildCount(); ++i) {
                //Find the child node containing the word "class" and use the next branch as the name
                ArrayList<String> leaves = leafSearch(classDeclarationNode.getChild(i), 1);
                if (leaves.size() == 1) {
                    if (leaves.get(0).equals("class") || leaves.get(0).equals("interface")) {
                        return classDeclarationNode.getChild(i + 1).getText();
                    }
                }
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            return "-NO NAME-";
        }
        return "-NO NAME-";
    }



    //Get the name of a method from a method declaration node
    private String getMethodName(ParserRuleContext methodDeclarationNode){
        try {
            String ruleName = parser.getRuleNames()[methodDeclarationNode.getRuleIndex()];

            if (!ruleName.equals("constructorDeclaration")) {
                return methodDeclarationNode.getChild(methodDeclarationNode.getChildCount() - 2).getChild(1).getChild(0).getText();
            } else {
                return methodDeclarationNode.getChild(methodDeclarationNode.getChildCount() - 2).getChild(0).getText();
            }
        }catch(NullPointerException e){
            e.printStackTrace();
            return "-NO NAME-";
        }
    }

    //Find the first ancestor node of the given node with one of the given rule names
    private ParserRuleContext findFirstParentRule(ParserRuleContext tree, String... rules){
        String treeName = parser.getRuleNames()[tree.getRuleIndex()];

        while (true){

            if (tree.equals(tree.getStart())){
                return null;
            }

            //Check if any of the rules match
            for (String rule : rules){
                if (treeName.equals(rule)){
                    return tree;
                }
            }

            tree = tree.getParent();

            if (tree == null){
                break;
            }

            treeName = parser.getRuleNames()[tree.getRuleIndex()];
        }

        return tree;
    }

    //Adds a class to the source package if it has been created
    private void addClass(ClassInstance newClass){
        if (sourcePackage == null){
            sourcePackage = new PackageInstance("-NO PACKAGE-");
        }

        sourcePackage.addClass(newClass);
        newClass.setParentPackage(sourcePackage);
        classStructure.add(newClass);
    }

    //Create a class instance at a class declaration node
    @Override
    public void enterNormalClassDeclaration(Java8Parser.NormalClassDeclarationContext ctx) {
        String className = getClassName(ctx);
        System.out.println("Found class: " + className);
        ClassInstance newClass = new ClassInstance(className);
        addClass(newClass);

    }

    //Create class instance for interfaces and add interface trait as a class modifier
    @Override
    public void enterNormalInterfaceDeclaration(Java8Parser.NormalInterfaceDeclarationContext ctx) {
        String className = getClassName(ctx);
        System.out.println("Found interface: " + className);
        ClassInstance newClass = new ClassInstance(className);
        newClass.addModifier("interface");
        addClass(newClass);
    }


    //Get the starting position of the method body in-order to count
    //the number of non-comment lines in the method
    @Override
    public void enterMethodBody(Java8Parser.MethodBodyContext ctx){
        findParentMethod(ctx).setNumLines(readBody(ctx.getStart().getLine()));
    }


    //Get the starting position of the constructor body in-order to count
    //the number of non-comment lines in the constructor
    @Override
    public void enterConstructorBody(Java8Parser.ConstructorBodyContext ctx){
        findParentMethod(ctx).setNumLines(readBody(ctx.getStart().getLine()));
    }


    //Read the number of lines in a body starting at the given line number including comments but not white spaces
    private int readBody(int startLine){
        int bodyLines = 0;

        try {
            //Open the source java file containing the body
            File sourceFile = new File(sourceFileName);

            Scanner fileReader = new Scanner(sourceFile);

            int bodyDepth = 0;
            boolean startFound = false;
            boolean inCommentBody = false;
            while (fileReader.hasNextLine()) {
                //Skip lines until the body is found
                if (startLine > 1){
                    fileReader.nextLine();
                    --startLine;
                }else{
                    //Count each line of code in the body
                    String codeLine = fileReader.nextLine().trim();

                    if (codeLine.startsWith("//")){continue;}

                    boolean inString = false;
                    char[] characters = codeLine.toCharArray();
                    StringBuilder builder = new StringBuilder(codeLine);

                    String newCodeLine = "";
                    //delete any string contents in source code
                    for (int i=0; i < codeLine.length(); i++){
                        if (characters[i] == '\"' || characters[i] == '\''){
                            if (i == 0) {inString = !inString;}
                            else if (characters[i-1] != '\\'){inString = !inString;}
                        }
                        if (!inString){newCodeLine += characters[i];}
                    }
                    codeLine = newCodeLine;

                    newCodeLine = "";
                    //Identify if the code is in a comment body
                    for (int i=0; i < codeLine.length(); i++){
                        if (i < codeLine.length() -1){
                            if (characters[i] == '/' && characters[i+1] == '*'){
                                inCommentBody = true;
                                ++i;
                                continue;
                            }
                            if (characters[i] == '*' && characters[i+1] == '/'){
                                inCommentBody = false;
                                ++i;
                                continue;
                            }
                        }

                        if (!inCommentBody){ newCodeLine += characters[i]; }
                    }

                    codeLine = newCodeLine;

                    if (codeLine.length() == 0){continue;}

                    ++bodyLines;

                    //Handle abstract or template methods by checking for a method body
                    if (startFound == false && codeLine.contains(";")){return 1;}

                    //Start of body found
                    if (codeLine.contains("{")){startFound = true;}

                    //Calculate the body depth (number of '{' to '}')
                    bodyDepth += codeLine.length() - codeLine.replace("{","").length();
                    bodyDepth -= codeLine.length() - codeLine.replace("}","").length();

                    //Body ends when there are equal or more '}' characters than '{' characters
                    //Dont include initial declaration
                    if (bodyDepth <= 0 && startFound){return --bodyLines;}

                    --startLine;
                }
            }

            fileReader.close();
        }catch (FileNotFoundException e){
            System.out.println("Listener failed to find source file: " + sourceFileName);
        }

        return --bodyLines;
    }


    //Check if the given method context is declared as part of the parent class or is a implementation for
    //a object instance
    private boolean isDeclaredMethod(ParserRuleContext ctx){
        ParserRuleContext parent = findFirstParentRule(ctx, "classInstanceCreationExpression_lfno_primary",
                "classDeclaration");

        if (parser.getRuleNames()[parent.getRuleIndex()].equals("classDeclaration")){return true;}
        return false;
    }

    //Create a new method instance at a method declaration node and add it to the parent class
    @Override
    public void enterMethodDeclaration(Java8Parser.MethodDeclarationContext ctx) {
        String methodName = getMethodName(ctx);

        MethodInstance newMethod = new MethodInstance(methodName);
        ClassInstance parentClass = findParentClass(ctx);
        if (!isDeclaredMethod(ctx)){newMethod.setDeclared(false);}
        parentClass.addMethod(newMethod);
    }


    //Create a new method instance for methods in an interface and add it to the parent interface instance
    @Override
    public void enterInterfaceMethodDeclaration(Java8Parser.InterfaceMethodDeclarationContext ctx) {
        String methodName = getMethodName(ctx);
        MethodInstance newMethod = new MethodInstance(methodName);

        ClassInstance parentClass = findParentClass(ctx);
        parentClass.addMethod(newMethod);
    }


    //Create a new method instance for a constructor and add it to the parent instance
    @Override
    public void enterConstructorDeclaration(Java8Parser.ConstructorDeclarationContext ctx) {
        String methodName = getMethodName(ctx);
        MethodInstance newMethod = new MethodInstance(methodName);

        ClassInstance parentClass = findParentClass(ctx);
        if (!isDeclaredMethod(ctx)){newMethod.setDeclared(false);}
        parentClass.addMethod(newMethod);
    }


    //Track statements do statements as branches in the code path of the parent method
    @Override
    public void enterDoStatement(Java8Parser.DoStatementContext ctx) {
        findParentMethod(ctx).addLogicalOperation();
    }

    //Track statements while statements as branches in the code path of the parent method
    @Override
    public void enterWhileStatement(Java8Parser.WhileStatementContext ctx) {
        findParentMethod(ctx).addLogicalOperation();
    }

    //Track statements for statements as branches in the code path of the parent method
    @Override
    public void enterForStatement(Java8Parser.ForStatementContext ctx) {
        findParentMethod(ctx).addLogicalOperation();
    }


    //Track statements If Then Else statements as branches in the code path of the parent method
    @Override
    public void enterIfThenElseStatement(Java8Parser.IfThenElseStatementContext ctx) {
        findParentMethod(ctx).addLogicalOperation();

    }

    //Track  If Then statements as branches in the code path of the parent method
    @Override
    public void enterIfThenStatement(Java8Parser.IfThenStatementContext ctx) {
        findParentMethod(ctx).addLogicalOperation();

    }

    //Track  try and catch statements as branches in the code path of the parent method
    @Override
    public void enterTryStatement(Java8Parser.TryStatementContext ctx){
        findParentMethod(ctx).addLogicalOperation();
    }


    //Track case statements as branches in the code path of the parent method
    @Override
    public void enterSwitchBlockStatementGroup(Java8Parser.SwitchBlockStatementGroupContext ctx) {
        findParentMethod(ctx).addLogicalOperation();
    }


    //Adds the found class modifier to the parent class instance
    @Override
    public void enterClassModifier(Java8Parser.ClassModifierContext ctx){
        findParentClass(ctx).addModifier(ctx.getText());
    }


    //Adds the found class modifier to the parent class instance
    @Override
    public void enterInterfaceModifier(Java8Parser.InterfaceModifierContext ctx){
        findParentClass(ctx).addModifier(ctx.getText());
    }


    //Adds the found method modifier to the parent method instance
    @Override
    public void enterMethodModifier(Java8Parser.MethodModifierContext ctx){
        findParentMethod(ctx).addModifier(ctx.getText());
    }


    //Adds the found interface method modifier to the parent method instance
    @Override
    public void enterInterfaceMethodModifier(Java8Parser.InterfaceMethodModifierContext ctx){
        findParentMethod(ctx).addModifier(ctx.getText());
    }

    //Adds the found constructor modifier to the parent method
    @Override
    public void enterConstructorModifier(Java8Parser.ConstructorModifierContext ctx){
        findParentMethod(ctx).addModifier(ctx.getText());
    }

    //Adds class type variables in class or method bodies as class dependencies
    @Override
    public void enterClassType(Java8Parser.ClassTypeContext ctx) {
        findParentClass(ctx).addDependencyName(ctx.getText());

    }

    //Adds inherited or implemented classes or interfaces as class dependencies
    @Override
    public void enterUnannClassOrInterfaceType(Java8Parser.UnannClassOrInterfaceTypeContext ctx) {
        findParentClass(ctx).addDependencyName(ctx.getText());
    }


    //Increment the number of parameters for the parent method
    @Override
    public void enterFormalParameter(Java8Parser.FormalParameterContext ctx){
        if (!parser.getRuleNames()[ctx.getParent().getParent().getParent().getRuleIndex()].equals("lambdaParameters")){
            findParentMethod(ctx).incrementArgument();
        }
    }


    //Create a package representation on a package declaration
    @Override
    public void enterPackageName(Java8Parser.PackageNameContext ctx){
        sourcePackage = new PackageInstance(ctx.getText());
    }

}
