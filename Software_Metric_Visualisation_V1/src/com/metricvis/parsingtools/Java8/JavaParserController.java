package com.metricvis.parsingtools.Java8;

import com.metricvis.instances.ClassInstance;
import com.metricvis.instances.PackageInstance;
import com.metricvis.parsingtools.Java8.generated.Java8Lexer;
import com.metricvis.parsingtools.Java8.generated.Java8Parser;
import com.metricvis.softwareversions.Milestone;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;


//Wrapper class for the Java8Parser object used to parse JAVA source code to generate a list of component representations
public class JavaParserController {

    //List of packages referenced in the input java source code
    private ArrayList<PackageInstance> packageList = new ArrayList<PackageInstance>();

    //List of all classes found in the input java source code including classes that don't have package parents
    private ArrayList<ClassInstance> classList = new ArrayList<ClassInstance>();

    //Number of total non-empty lines and comment lines in the given java source file
    private int numLines = 0;
    private int numCommentLines = 0;

    //Get the total number of non-empty lines
    public int getNumLines(){
        return numLines;
    }

    //Get the number of comment lines
    public int getNumCommentLines(){
        return numCommentLines;
    }

    //Creates and walks through the parse tree creating representation of each component in the source code
    public Milestone runParser(String directoryName) {
        classList.clear();
        packageList.clear();
        ArrayList<String> javaFiles = findJavaFiles(directoryName);

        if (javaFiles.size() == 0){
            System.out.println("No Java files found in directory: " + directoryName);
            return getMilestone();
        }

        ParseTreeWalker walker = new ParseTreeWalker();

        for (String fileName : javaFiles) {
            try {
                //Attempt to open the given source code file
                CharStream stream = CharStreams.fromFileName(fileName);
                //Build the lexer and get the contained token rules for the language
                Java8Lexer javaLexer = new Java8Lexer(stream);
                CommonTokenStream commonTokenStream = new CommonTokenStream(javaLexer);

                //Parser object for recognising java code rules
                Java8Parser parser = new Java8Parser(commonTokenStream);
                //Java parser listener used to react to rules in the parse tree and build the class representations
                JavaListener listener = new JavaListener(parser, fileName);;
                //Top node of the generated parse tree
                ParserRuleContext treeTop = parser.compilationUnit();;

                //Walk though the parse tree triggering listener events
                walker.walk(listener, treeTop);

                if (!listener.getSourcePackage().equals(null)){addPackage(listener.getSourcePackage());}


                classList.addAll(listener.getClasses());

                //Remove any methods that were declared within functions
                for (ClassInstance cls : classList){
                    cls.removeUndeclaredMethods();
                }
                countLines(fileName);

            } catch (IOException e) {
                System.out.println("Failed to open file: " + fileName);
                e.printStackTrace();
            }
        }

        buildDependencies();

        return getMilestone();
    }


    //Get a list of java file names in the given directory
    public ArrayList<String> findJavaFiles(String directoryName){
        ArrayList<String> javaFiles = new ArrayList<String>();

        File directory = new File(directoryName);
        FileNameExtensionFilter javaFilter = new FileNameExtensionFilter("Java File","java");

        //Check if file path is a folder or file
        if (directory.isDirectory()){
            //Look for java files or folders in the folder
            for (File file : directory.listFiles())
            {
                try {
                    if (file.isDirectory()){
                        javaFiles.addAll(findJavaFiles(file.getCanonicalPath()));
                    }else if (javaFilter.accept(file)){
                        javaFiles.add(file.getCanonicalPath());
                    }
                }catch (IOException e){
                    System.out.println("Failed to open file: " + file.getName());
                    e.printStackTrace();
                }
            }
        }else if(javaFilter.accept(directory)){
            //Path is a file so check if it is a java file
            try {
                javaFiles.add(directory.getCanonicalPath());
            }catch (IOException e) {
                System.out.println("Failed to open file: " + directory.getName());
                e.printStackTrace();
            }
        }

        return javaFiles;
    }


    //Add a package to the package list and update a package with a matching name
    private void addPackage(PackageInstance newPackage){
        for (int i=0; i < packageList.size(); ++i){
            if (packageList.get(i).getName().equals(newPackage.getName())){
                for (ClassInstance newClass : newPackage.getClassList()){
                    packageList.get(i).addClass(newClass);
                }
                return;

            }
        }
        packageList.add(newPackage);
    }


    //Use the string values of the class dependency names to create a list of outgoing and incoming dependencies for each class
    private void buildDependencies(){
        for (int i=0; i< classList.size(); ++i){
            ClassInstance currentClass = classList.get(i);
            //Get the name of each class dependency
            ArrayList<String> dependencyNames = currentClass.getDependencyNames();

            //Find the class with the matching name
            for (String dependency : currentClass.getDependencyNames()){
                for (int j=0; j < classList.size(); ++j){
                    if (classList.get(j).getName().equals(dependency)){
                        //Add to current class as a incoming dependency
                        currentClass.addOutgoingDependency(classList.get(j));
                        //Add to other class as outgoing dependency
                        classList.get(j).addIncomingDependency(currentClass);
                        break;
                    }
                }
            }
        }
    }


    //Get the created package representation from the listener
    public ArrayList<PackageInstance> getPackages(){
        return packageList;
    }


    //Get a list of class representations generated after parsing
    public Milestone getMilestone(){
        Milestone projectMilestone = new Milestone(new Date());
        ArrayList<ClassInstance> milestoneClasses = new ArrayList<ClassInstance>();
        milestoneClasses.addAll(classList);
        ArrayList<PackageInstance> milestonePackages = new ArrayList<PackageInstance>();
        milestonePackages.addAll(packageList);
        projectMilestone.setClasses(milestoneClasses);
        projectMilestone.setPackages(milestonePackages);
        return projectMilestone;
    }


    //Counts the number of non-empty lines and comment lines in the source file
    private void countLines(String fileName){

        try {
            File sourceFile = new File(fileName);

            Scanner fileReader = new Scanner(sourceFile);

            //Flag to indicate line is inside block comment
            boolean isInComment = false;

            //Read each line in the file disregarding code rules
            while (fileReader.hasNextLine()) {
                String codeLine = fileReader.nextLine();
                if (!codeLine.isEmpty()){++numLines;}
                if (codeLine.contains("//")){++numCommentLines;}

                //Count comment lines in a block comment
                if (codeLine.contains("/*") && !isInComment){ isInComment = true;}
                if (isInComment){++numCommentLines;}
                if (codeLine.contains("*/") && isInComment ){isInComment= false;}

            }

            fileReader.close();

        }catch(FileNotFoundException e){
            System.out.println("Failed to locate file: " + fileName);
            e.printStackTrace();
        }

    }



    //Show the constructed parse tree in a JFrame window
    //Scale argument modifies the size of the JFrame
    public static void showTree(String windowName, String javaFile, float scale) {
        JFrame frame = new JFrame(windowName);
        JPanel panel = new JPanel();

        try {
            //Attempt to open the given source code file
            CharStream stream = CharStreams.fromFileName(javaFile);
            //Build the lexer and get the contained token rules for the language
            Java8Lexer javaLexer = new Java8Lexer(stream);
            CommonTokenStream commonTokenStream = new CommonTokenStream(javaLexer);

            //Parser object for recognising java code rules
            Java8Parser parser = new Java8Parser(commonTokenStream);

            //Create tree view graphic
            TreeViewer treeView = new TreeViewer(Arrays.asList(parser.getRuleNames()), parser.compilationUnit());
            treeView.setScale(scale);
            panel.add(treeView);
            frame.add(panel);
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            frame.pack();
            frame.setVisible(true);

        }catch(IOException e){
            System.out.println("Couldn't open file: " + javaFile);
            e.printStackTrace();
        }
    }

}
