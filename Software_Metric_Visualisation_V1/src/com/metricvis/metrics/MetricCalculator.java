package com.metricvis.metrics;

import com.metricvis.instances.ClassInstance;
import com.metricvis.instances.MethodInstance;
import com.metricvis.instances.PackageInstance;

import java.util.ArrayList;



//Class containing static methods used to calculate software metrics for the given class or package
public class MetricCalculator {

    //Get a list of available class metric values
    public static ArrayList<ClassMetric> getClassMetrics(){
        return classMetricList;
    }


    //Get a list of available dependency metric values
    public static ArrayList<DependencyMetric> getDependencyMetrics(){
        return dependencyMetricList;
    }


    //Get a list of available package metric values
    public static ArrayList<PackageMetric> getPackageMetrics(){
        return packageMetricList;
    }

    //Blank dependency metric, always returns target value
    public static DependencyMetric NoDependencyMetric = new DependencyMetric("No Dependency Metric", 1) {
        @Override
        protected float calculateValue(ClassInstance instance, int dependencyID) { return 1; }
    };


    //Calculate number of references to each outgoing dependency
    public static DependencyMetric NumReferences = new DependencyMetric("Number Of References", 3) {
        @Override
        protected float calculateValue(ClassInstance instance, int dependencyID) {
            return instance.getOutDependencyOccurances().get(dependencyID);
        }
    };


    //Calculates how far each number of references to a dependency is from the class wise average
    public static DependencyMetric NumReferencesDeviation = new DependencyMetric("References Deviation", 0) {
        @Override
        protected float calculateValue(ClassInstance instance, int dependencyID) {
            float averageNumReferences = 0;

            if (instance.getOutDependencyOccurances().size() == 0){
                setTarget(0);
                return 0;
            }

            for (int i=0; i < instance.getOutDependencyOccurances().size(); ++i){
                averageNumReferences += instance.getOutDependencyOccurances().get(i);
            }
            averageNumReferences /= instance.getOutDependencyOccurances().size();
            setTarget(averageNumReferences);

            return instance.getOutDependencyOccurances().get(dependencyID);
        }
    };


    //Recursive function which returns true if the origin node is part of a dependency cycle
    public static boolean checkForCycle(ClassInstance origin, ClassInstance cycleNode, int maxDepth){
        if (cycleNode.equals(origin)){ return true;}
        if (maxDepth <= 0){return false;}

        for (ClassInstance dependency : cycleNode.getOutgoingDependencies()){
            if (checkForCycle(origin, dependency, maxDepth-1)){ return true; }
        }

        return false;
    }


    //Sets the metric value for dependencies in a cycle to 100, otherwise 1
    //The search depth of the dependency tree is limited to 20 levels
    public static DependencyMetric isInCycle = new DependencyMetric("Show Cycle", 1) {
        @Override
        protected float calculateValue(ClassInstance instance, int dependencyID) {
            if (checkForCycle(instance, instance.getOutgoingDependencies().get(dependencyID),12)){
                return 100;
            }
            return 1;
        }
    };


    //Sets the metric value for dependencies in a cycle to 100, otherwise 1
    //Only checks for cycles containing only two classes
    public static DependencyMetric isInImmediateCycle = new DependencyMetric("Show Immediate Cycle", 1) {
        @Override
        protected float calculateValue(ClassInstance instance, int dependencyID) {
            ClassInstance dependantClass = instance.getOutgoingDependencies().get(dependencyID);
            if (checkForCycle(instance,dependantClass,1)){
                return 100;
            }
            return 1;
        }
    };


    //Blank class metric, always returns target value
    public static ClassMetric NoClassMetric = new ClassMetric("No Class Metric",1){
        @Override
        public float calculateValue(ClassInstance instance) { return 1; }
    };


    //Calculate number of methods in the class
    public static ClassMetric NumberOfMethods = new ClassMetric("Number Of Methods",10){
        @Override
        public float calculateValue(ClassInstance instance) { return instance.getNumMethods(); }
    };

    //Calculate class instability
    public static ClassMetric ClassInstability = new ClassMetric("Class Instability",51){
        @Override
        public float calculateValue(ClassInstance instance) {
            float classInstability = 0;
            float totalDependencies = instance.getIncomingDependencies().size() + instance.getOutgoingDependencies().size();
            if (totalDependencies > 0){
                classInstability = instance.getOutgoingDependencies().size() / totalDependencies;
            }
            return (classInstability * 100) + 1;
        }
    };



    //Calculate the average complexity of methods in the given class
    public static ClassMetric AverageMethodComplexity = new ClassMetric("Average Method Complexity",4){
        @Override
        public float calculateValue(ClassInstance instance) {
            float avgComplexity = 0;

            ArrayList<MethodInstance> methods = instance.getMethods();
            if (methods.size() > 0) {
                for (MethodInstance methodInstance : methods) {
                    avgComplexity += methodInstance.getComplexity();
                }
                if (avgComplexity == 0){return 0;}
                avgComplexity /= methods.size();
            }

            return avgComplexity;
        }
    };


    //Calculate the highest method complexity of the methods in the given class
    public static ClassMetric HighestMethodComplexity = new ClassMetric("Highest Method Complexity",5){
        @Override
        public float calculateValue(ClassInstance instance) {
            float highestComplexity = 0;

            ArrayList<MethodInstance> methods = instance.getMethods();

            for (MethodInstance methodInstance : methods){
                if (methodInstance.getComplexity() > highestComplexity){
                    highestComplexity = methodInstance.getComplexity();
                }
            }

            return highestComplexity;
        }
    };


    //Calculate the average number of method parameters for methods in the given class
    public static ClassMetric AverageMethodParameters = new ClassMetric("Average Method Parameters",2.5f){
        @Override
        public float calculateValue(ClassInstance instance) {
            float avgParameters = 0;

            ArrayList<MethodInstance> methods = instance.getMethods();

            if (methods.size() > 0) {

                for (MethodInstance method : methods) {
                    avgParameters += method.getNumArguments();
                }

                if (avgParameters == 0){return 0;}
                avgParameters /= methods.size();
            }
            return avgParameters;
        }
    };


    //Find the highest number of parameters required my a method in the given class
    public static ClassMetric HighestMethodParameters = new ClassMetric("Highest Method Parameters",4){
        @Override
        public float calculateValue(ClassInstance instance) {
            float highestParameters = 0;
            for (MethodInstance method : instance.getMethods()){

                if (method.getNumArguments() > highestParameters){
                    highestParameters = method.getNumArguments();
                }
            }
            return highestParameters;
        }
    };


    //Calculate the average number of lines in the body of the methods in the given class
    public static ClassMetric AverageMethodSize = new ClassMetric("Average Method Size",13){
        @Override
        public float calculateValue(ClassInstance instance) {
            float averageSize = 0;
            ArrayList<MethodInstance> methods = instance.getMethods();

            if (methods.size() > 0) {

                for (MethodInstance method : methods) {
                    averageSize += method.getNumLines();
                }

                if (averageSize == 0){return 0;}

                averageSize /= methods.size();
            }
            return averageSize;
        }
    };


    //Find the highest number of lines in a body of a method in the given class
    public static ClassMetric HighestMethodSize = new ClassMetric("Highest Method Size",17){
        @Override
        public float calculateValue(ClassInstance instance) {
            float highestSize = 0;


            for (MethodInstance method : instance.getMethods()) {
                if (method.getNumLines() > highestSize){
                    highestSize = method.getNumLines();
                }
            }
            return highestSize;
        }
    };



    public static PackageMetric PackageAbstractness = new PackageMetric("Package Abstractness",0.5f) {
        @Override
        public float calculateValue(PackageInstance instance) {
            float abstractness = 0;
            float abstractClasses = 0;
            ArrayList<ClassInstance> classList = instance.getClassList();

            for (ClassInstance classInstance : classList) {
                if (classInstance.getModifiers().contains("abstract") || classInstance.getModifiers().contains("interface")) {
                    ++abstractClasses;
                }
            }


            if (classList.size() > 0){
                abstractness = abstractClasses / classList.size();
            }
            return abstractness;
        }
    };



    //Calculate package instability using the dependencies of the contained classes
    public static PackageMetric PackageInstability = new PackageMetric("Package Instability",-1) {
        @Override
        public float calculateValue(PackageInstance instance) {
            float packageInstability = 0;

            float outgoingDependencies = 0;
            float incomingDependencies = 0;

            for (ClassInstance classInstance : instance.getClassList()){
                //Get all out-going dependencies for this class which our outside this package/component
                for (ClassInstance outDependency : classInstance.getOutgoingDependencies()){
                    if (!outDependency.getParentPackage().equals(instance)){
                        ++outgoingDependencies;
                    }
                }

                //Get all incoming dependencies for this class which our outside this package/component
                for (ClassInstance inDependency : classInstance.getIncomingDependencies()){
                    if (!inDependency.getParentPackage().equals(instance)){
                        ++incomingDependencies;
                    }
                }
            }

            float totalDependencies = outgoingDependencies + incomingDependencies;

            if (totalDependencies > 0){
                packageInstability = outgoingDependencies / totalDependencies;
            }
            return packageInstability;
        }
    };



    //Calculate the ratio of total non-emptry code lines to lines of comment
    public static SoftwareMetric calculateCommentToCodeRatio(int numComments, int totalLines){
        return new SoftwareMetric("Comment To Code Ratio", (float)numComments / (float)totalLines, 0.08f);
    }

    //List of available per-class metrics
    private static final ArrayList<ClassMetric> classMetricList = new ArrayList<ClassMetric>() {
        {
            add(NoClassMetric);
            add(ClassInstability);
            add(NumberOfMethods);
            add(AverageMethodComplexity);
            add(HighestMethodComplexity);
            add(AverageMethodParameters);
            add(HighestMethodParameters);
            add(AverageMethodSize);
            add(HighestMethodSize);
        }
    };


    //List of available per-dependency metrics
    private static final ArrayList<DependencyMetric> dependencyMetricList = new ArrayList<DependencyMetric>() {
        {
            add(NoDependencyMetric);
            add(NumReferences);
            add(NumReferencesDeviation);
            add(isInCycle);
            add(isInImmediateCycle);
        }
    };

    //List of available per-package metrics
    private static final ArrayList<PackageMetric> packageMetricList = new ArrayList<PackageMetric>() {
        {
            add(PackageInstability);
            add(PackageAbstractness);
        }
    };

}

