package com.metricvis.metrics;

import com.metricvis.instances.ClassInstance;

import java.util.ArrayList;

//Representation of per-outward dependency metric
public abstract class DependencyMetric extends SoftwareMetric{

    public DependencyMetric(String name, float target) {
        super(name, 0.0f, target);
    }

    //Returns a list of SoftwareMetric instances (one for each outwards dependency)
    public ArrayList<SoftwareMetric> calculateMetrics(ClassInstance instance){
        ArrayList<SoftwareMetric> dependencyMetrics = new ArrayList<SoftwareMetric>();

        for(int i=0; i < instance.getOutgoingDependencies().size(); ++i){
            dependencyMetrics.add(new SoftwareMetric(getName(),calculateValue(instance, i),getTarget()));
        }
        return dependencyMetrics;
    }

    //Function used called for each outward dependency to create a list of unique values
    protected abstract float calculateValue(ClassInstance instance, int dependencyID);
}
