package com.metricvis.metrics;

import com.metricvis.instances.PackageInstance;

//Representation of per-package metrics
public abstract class PackageMetric extends SoftwareMetric{

    public PackageMetric(String name, float target) {
        super(name, 0.0f, target);
    }

    //Returns a SoftwareMetric instance containing the metric target and name
    public SoftwareMetric calculateMetric(PackageInstance instance){
        return new SoftwareMetric(getName(),calculateValue(instance), getTarget());
    }

    //Per metric implementation of value calculation
    protected abstract float calculateValue(PackageInstance instance);
}