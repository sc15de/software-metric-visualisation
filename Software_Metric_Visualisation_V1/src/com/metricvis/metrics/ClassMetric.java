package com.metricvis.metrics;

import com.metricvis.instances.ClassInstance;


//Representation of per-class metrics
public abstract class ClassMetric extends SoftwareMetric{

    public ClassMetric(String name, float target) {
        super(name, 0.0f, target);
    }

    //Returns a SoftwareMetric instance containing the metric target and name
    public SoftwareMetric calculateMetric(ClassInstance instance){
        return new SoftwareMetric(getName(),calculateValue(instance), getTarget());
    }

    //Per metric implementation of value calculation
    protected abstract float calculateValue(ClassInstance instance);
}