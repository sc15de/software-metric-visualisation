package com.metricvis.metrics;


//Class representation for a software metric containing both the
//metric value and target value
public class SoftwareMetric {
    //Metric value
    private float value;
    //Metric target value, should be -1 for no valid target
    private float target;

    //Name of the software metric
    private String name;

    //Create the software metric instance with a name, value and a target value
    public SoftwareMetric(String metricName, float metricValue, float metricTarget) {
        value = metricValue;
        target = metricTarget;
        name = metricName;
    }

    public void setTarget(float newTarget){
        target = newTarget;
    }

    //Get the software metric target value
    public float getTarget() {
        return target;
    }

    //Get the software metric value
    public float getValue() {
        return value;
    }

    //Get the name of the metric
    public String getName(){
        return name;
    }


    //Print friendly description of the software metric
    @Override
    public String toString(){
        if (target == -1){
            return getName() + ": Value: " + getValue() + ", Target: NONE";
        }
        return getName() + ": Value: " + getValue() + ", Target: " + getTarget();
    }

}
