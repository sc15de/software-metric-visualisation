package com.metricvis;

import com.metricvis.gui.MainWindow;


//Main class of Software Metric Visualisation
public class Main {

    //Entry point of application to open the main application window
    public static void main(String[] args) {
        MainWindow window = new MainWindow();
        window.start();
    }
}
